
-----------------------------------------------------
--[[  Name: Variable registration.
      Description: Following convention here and registering the two variables I have used. ]]--

DarkRP.registerDarkRPVar("DarkRP_LockDown", function(val) net.WriteBool(val) end, function() return net.ReadBool() end)
DarkRP.registerDarkRPVar("tcb_Stamina", function(val) net.WriteInt(val, 32) end, function() return net.ReadInt(32) end)