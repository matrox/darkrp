
-----------------------------------------------------
local config = {}

--[[  Name: Enable addons.
      Description: Set to either true/false depending on if your server has the respective addons installed/enabled.
      Note: Read the instructions on scriptfodder to properly set these up. ]]--

config.hunger = false -- You must enable this in addons/darkrpmodification/lua/darkrp_config/disabled_defaults.lua on line 32.
config.stamina = false -- You can find this here: https://github.com/TheCodingBeast/TCB_Stamina
config.vrondakis_leveling_system = false -- You can find this here: https://github.com/vrondakis/DarkRP-Leveling-System
config.netheous_levelup = false -- You can find this here: https://scriptfodder.com/scripts/view/801
config.organisation = false -- You can find this here: https://scriptfodder.com/scripts/view/317

-- Note: Please only set "config.netheous_levelup" OR "config.vrondakis_leveling_system" to true. If you set them both to true then it will not display properly.

--[[  Name: Stat bars will pulse when they have low values.
      Description: Health bars will flash quicker the lower health you have. ]]--
      
config.bar_pulse = true

--[[  Name: Player model preview panel.
      Description: Enable/disable the preview or show Steam avatars instead? ]]--
      
config.preview_enabled = true -- Show the player preview?
config.preview_avatar = true -- Show Steam avatars instead of player models? (Disregarded if "config.preview_enabled = false")
config.preview_wanted = false -- Show an exclamation mark over your Steam avatar when you are wanted as an additional visual cue. (Disregarded if "config.preview_avatar = false")

--[[  Name: Left sub-heading on the main panel.
      Description: Display usergroup instead of job title? ]]--
       
config.show_usergroup = false

--[[  Name: Entity display layout.
      Description: Changes the layout of the information above the heads of players. ]]--

config.entity_display_offset = 15 -- Position of the information above player's heads. (15 seems optimal)
config.entity_license_above = true -- Moves the license to be above their name instead of to the right of it.
config.entity_health_name = true -- Have player health included with their name instead of a separate line?
config.entity_show_level = true -- Display the level of players if you have the experience addon enabled?

--[[  Name: License display.
      Description: Show the license over the preview panel instead of being at the top right? ]]--
      
config.license_over_preview = true

--[[  Name: Ammo display.
      Description: Enable ammo being displayed and change the x offset. ]]--
       
config.ammo = true -- Set this to false if you do not want ammo to be displayed.
config.x_ammo = 0 -- 0 is the default value, change to a negative number to make the ammo display move to the left.
config.y_ammo = 0 -- 0 is the default value, change to a negative number to make the ammo display move upwards.

--[[  Name: Weapon pickup manifest.
      Description: Enable the background for the weapon pickup display? ]]--
       
config.pickup_background = true

--[[  Name: Text that appears during a Lockdown.
      Description: Changes the text that displays in the center of the screen. ]]--
      
config.lockdown_heading = "Burmistrz oglosil godzine policyjna!"
config.lockdown_subheading = "Prosimy natychmiast wrocic do domow."

--[[  Name: Colour spectrum.
      Description: Change the colours below to suit your preference. ]]--

local colour = {
   ["pure_white"] = Color(255, 255, 255),
   ["white"] = Color(220, 220, 220),
   ["grey"] = Color(155, 155, 155),
   ["darkest"] = Color(43, 49, 55),
   ["dark"] = Color(55, 61, 67),
   ["light"] = Color(101, 111, 123),
   ["bar"] = Color(62, 88, 99),
   ["health"] = Color(231, 76, 60),
   ["stamina"] = Color(241, 196, 15),
   ["hunger"] = Color(230, 126, 34),
   ["experience"] = Color(46, 204, 113),
   ["armour"] = Color(52, 152, 219),
   ["entity_health"] = Color(255, 255, 255),
   ["entity_job"] = Color(220, 220, 220),
   ["entity_organisation"] = Color(200, 200, 200),
   ["entity_level"] = Color(230, 230, 230)
}

--[[  Name: End of config.
      Description: Do not edit anything below this comment, unless you know exactly what you are doing. ]]--

local surface = surface

surface.CreateFont("open_sans_64b", {font = "Open Sans Bold", size = 64, weight = 800, antialias = true})
surface.CreateFont("open_sans_64b_shadow", {font = "Open Sans Bold", size = 64, weight = 800, antialias = false, outline = true, blursize = 1})
surface.CreateFont("open_sans_42b", {font = "Open Sans Bold", size = 42, weight = 800, antialias = true})
surface.CreateFont("open_sans_42b_shadow", {font = "Open Sans Bold", size = 42, weight = 800, antialias = false, outline = true, blursize = 1})
surface.CreateFont("open_sans_25b", {font = "Open Sans Bold", size = 25, weight = 800, antialias = true})
surface.CreateFont("open_sans_25b_shadow", {font = "Open Sans Bold", size = 25, weight = 800, antialias = false, outline = true, blursize = 1})
surface.CreateFont("open_sans_19b", {font = "Open Sans Bold", size = 19, weight = 800, antialias = true})
surface.CreateFont("open_sans_19b_shadow", {font = "Open Sans Bold", size = 19, weight = 800, antialias = false, outline = true, blursize = 1})
surface.CreateFont("open_sans_17b", {font = "Open Sans Bold", size = 17, weight = 800, antialias = true})
surface.CreateFont("open_sans_17b_shadow", {font = "Open Sans Bold", size = 17, weight = 800, antialias = false, outline = true, blursize = 1})
surface.CreateFont("open_sans_15", {font = "Open Sans Bold", size = 15, weight = 400, antialias = true})
surface.CreateFont("open_sans_15_shadow", {font = "Open Sans Bold", size = 15, weight = 400, antialias = false, outline = true, blursize = 1})
surface.CreateFont("tahoma_14", {font = "Tahoma", size = 14, weight = 400, antialias = true})
surface.CreateFont("tahoma_14_shadow", {font = "Tahoma", size = 14, weight = 400, antialias = false, outline = true, blursize = 1})
surface.CreateFont("tahoma_13b", {font = "Tahoma", size = 13, weight = 800, antialias = true})
surface.CreateFont("tahoma_13b_shadow", {font = "Tahoma", size = 13, weight = 800, antialias = false, outline = true, blursize = 1})
surface.CreateFont("tahoma_13", {font = "Tahoma", size = 13, weight = 400, antialias = true})
surface.CreateFont("tahoma_13_shadow", {font = "Tahoma", size = 13, weight = 400, antialias = false, outline = true, blursize = 1})

local string = string
local math = math
local draw = draw
local dst = draw.SimpleText
local plyMeta = FindMetaTable("Player")

local x = ScrW()
local y = ScrH()
local pos_x = 0
local pos_y = 0
local offset = 0
local position = 0
local alpha = 0
 
local icon_license = Material("icon16/page_white_text.png")
local icon_hunger = Material("icon16/cup.png")
local texture_voice = surface.GetTextureID("voice/icntlk_pl")
local texture_gradient = surface.GetTextureID("gui/gradient")
local salary, wallet, job

local experience_h_offset = 0
local preview_w_offset = -89

local Wallet, pWallet = 0, 0
local Hunger, pHunger = 0, 0
local Stamina, pStamina = 0, 0
local Experience, pExperience, pMaxExperience = 0, 0, 0
local Health, Armour = 0, 0
local MaxHealth, MaxArmour = 0, 0
local Preview
local EntityText = {}
local EntityColour

local function FormatString(str)
   local words = string.Explode(" ", str)
   local upper, lower, str
   local formatted = {}
   
   for k, v in pairs(words) do
      upper = string.upper(string.sub(v, 1, 1))
      lower = string.lower(string.sub(v, 2))
      table.insert(formatted, upper..lower)
   end
   
   str = formatted[1]

   if #formatted > 1 then
      for i = 1, #formatted-1 do
         str = str.." "..formatted[i+1]
      end
   end

   return str
end

local function FormatNumber(number)
	if !number then return "" end
	if number >= 1e14 then return tostring(number) end
   
   number = tostring(number)
    
   local sep = sep or ","
   local dp = string.find(number, "%.") or #number + 1
   
	for i = dp - 4, 1, -3 do
		number = number:sub(1, i) .. sep .. number:sub(i + 1)
   end
   
   return number
end

local function GetGroup(client)
   if !IsValid(client) then return end

   local str = FormatString(client:GetUserGroup())
   
   if string.find(str, "admin") then
      str = string.gsub(str, "admin", " Admin")
   end
   
   return str
end

local function GetPercentage(value, max_value, max_width)
   if (!max_width) then max_width = 100 end

   local result = math.Clamp((value / max_value) * (max_width), 0, (max_width))

   return math.floor(result)
end

local function GetPulse(colour, r, g, b, a, value)
   if (!config.bar_pulse) then return colour end

   local active = {}
   
   if (r) then table.insert(active, "red") end
   if (g) then table.insert(active, "green") end
   if (b) then table.insert(active, "blue") end
   if (a) then table.insert (active, "alpha") end
   
   if (#active > 1) then return colour end
   
   local intensity = 0.8
   local speed = math.Clamp((10 / value) * (10 / value) * 200, 1, 10)
   local pulse
   
   if (r) then pulse = Color(math.Clamp(math.sin(RealTime() * speed) * colour.r, (colour.r * intensity), colour.r), colour.g, colour.b) end
   if (g) then pulse = Color(colour.r, math.Clamp(math.sin(RealTime() * speed) * colour.g, (colour.g * intensity), colour.g), colour.b) end
   if (b) then pulse = Color(colour.r, colour.g, math.Clamp(math.sin(RealTime() * speed) * colour.b, (colour.b * intensity), colour.b)) end
   if (a) then pulse = Color(colour.r, colour.g, colour.b, math.Clamp(math.sin(RealTime() * speed) * colour.a, (colour.a * intensity), colour.a)) end
   
   return pulse
end

local function GetAmmo(client)
   local weapon = client:GetActiveWeapon()
   if !weapon or !client:Alive() then return -1 end

   local ammo_inv = weapon:Ammo1()
   local ammo_clip = weapon:Clip1()
   local ammo_max = weapon.Primary.ClipSize

   return ammo_clip, ammo_max, ammo_inv
end

local value = 0

local function GetTransition(from, to)
   value = value + (to / 4) * FrameTime()
   value = math.Clamp(value, from, to)
   
   return math.Round(value)
end

local function NormalRect(x, y, w, h, colour, highlight)
   surface.SetDrawColor(colour)
   surface.DrawRect(x, y, w, h)
   
   if (highlight) then
      surface.SetDrawColor(Color(colour.r * 1.2, colour.g * 1.2, colour.b * 1.2, colour.a))
      surface.DrawRect(x, y, w, 1)
      surface.DrawRect(x + w - 1, y, 1, h)
   end
end

local function BorderedRect(x, y, w, h, colour, override, highlight, up, down, left, right)
   surface.SetDrawColor(colour)
   surface.DrawRect(x, y, w, h)
   
   if (highlight) then 
      surface.SetDrawColor(Color(colour.r * 1.2, colour.g * 1.2, colour.b * 1.2, colour.a))
      surface.DrawRect(x, y + 1, w, 1)
      surface.DrawRect(x + w - 2, y, 1, h)
   end
   
   if (override) then colour = Color(43, 49, 55) end
      
   if (up) or (down) or (left) or (right) then 
      surface.SetDrawColor(Color(colour.r * 0.6, colour.g * 0.6, colour.b * 0.6, colour.a))
   end
   
   if (up) then surface.DrawRect(x, y, w, 1) end
   if (down) then surface.DrawRect(x, y + h - 2, w, 2) end
   if (left) then surface.DrawRect(x, y, 1, h) end
   if (right) then surface.DrawRect(x + w - 1, y, 1, h) end
end

local function HealthArmourBar(x, y, w, h, health, armour)
   local health = math.floor(health)
   local armour = math.floor(armour)
   
   NormalRect(x, y, health, h, GetPulse(colour.health, true, false, false, false, LocalPlayer():Health()), true)
   
   if (armour > 0) and LocalPlayer():Alive() then
      NormalRect(x + health, y, armour, h, colour.armour, true)
   end
end

local function BlurShadowText(text, font, x, y, colour, xalign, yalign)
   dst(text, font.."_shadow", x, y + 1, Color(0, 0, 0, 150), xalign, yalign)
   dst(text, font, x, y, colour, xalign, yalign)
end

local function SimpleShadowText(text, font, x, y, colour, min, xalign, yalign)
   dst(text, font, x + 1, y + 1, Color(0, 0, 0, math.min(colour.a, min + 70)), xalign, yalign)
   dst(text, font, x + 2, y + 1, Color(0, 0, 0, math.min(colour.a, min)), xalign, yalign)
   dst(text, font, x, y, colour, xalign, yalign)
end

local AdminTell = function() end

usermessage.Hook("AdminTell", function(msg)
   timer.Destroy("DarkRP_AdminTell")
	local text = msg:ReadString()

	AdminTell = function()
      BlurShadowText("Announcement", "open_sans_42b", x / 2, y / 2 - 230, colour.pure_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		BlurShadowText(text, "open_sans_25b", x / 2, y / 2 - 200, colour.white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	timer.Simple(10, function()
		AdminTell = function() end
	end)
end)

local Arrested = function() end

usermessage.Hook("GotArrested", function(msg)
   local StartArrested = CurTime()
   local ArrestedUntil = msg:ReadFloat()
   
	Arrested = function()
		if (CurTime() - StartArrested <= ArrestedUntil and LocalPlayer():getDarkRPVar("Arrested")) then
         SimpleShadowText("You arested for "..string.ToMinutesSeconds(math.ceil(ArrestedUntil - (CurTime() - StartArrested))).."!", "open_sans_25b", x / 2, 150, colour.pure_white, 100, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER) 
		elseif !LocalPlayer():getDarkRPVar("Arrested") then
			Arrested = function() end
		end
	end
end)

local function DrawLockdown(client)
   local cin = (math.sin(CurTime()) + 1) / 2

  	if (client:getDarkRPVar("DarkRP_LockDown")) then
      SimpleShadowText(config.lockdown_heading, "open_sans_25b", x / 2, 100, Color(cin * 255, 0, 255 - (cin * 255), 255), 180, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      SimpleShadowText(config.lockdown_subheading, "open_sans_19b", x / 2, 122, colour.white, 100, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
end

local function DrawVoice(client)
	if (client.DRPIsTalking) then
		local cx, cy = chat.GetChatBoxPos()
		local rotating = math.sin(CurTime() * 3)
		local backwards = 0
      
		if (rotating < 0) then
			rotating = 1 - (1 + rotating)
			backwards = 180
		end
      
		surface.SetTexture(texture_voice)
		surface.SetDrawColor(colour.health)
		surface.DrawTexturedRectRotated(ScrW() - 100, cy, rotating * 96, 96, backwards)
	end
end

local function DrawAgenda(client)
   local Agenda = client:getAgendaTable()
   
   if !Agenda then return end

   SimpleShadowText(FormatString(Agenda.Title), "open_sans_25b", 20, 10, colour.pure_white, 100, 0)
   
   local text = client:getDarkRPVar("agenda") or "No agenda set."
   text = text:gsub("//", "\n"):gsub("\\n", "\n")
   text = DarkRP.textWrap(text, "open_sans_19b", 456)
   
   if (text == "") then text = "No agenda set." end
   
   draw.DrawNonParsedText(text, "open_sans_19b", 20 + 1, 35 + 1, Color(0, 0, 0, 170), 0)
   draw.DrawNonParsedText(text, "open_sans_19b", 20 + 2, 35 + 2, Color(0, 0, 0, 100), 0)
   draw.DrawNonParsedText(text, "open_sans_19b", 20, 35, colour.white, 0)
end

local function DrawAmmo(client)
   if !IsValid(client:GetActiveWeapon()) then return end
   if (client:GetActiveWeapon():Clip1() == NULL or client:GetActiveWeapon() == "Camera") then return end
   
   local clip = client:GetActiveWeapon():Clip1()
   local reserve = client:GetAmmoCount(client:GetActiveWeapon():GetPrimaryAmmoType())
   
   if (client:GetActiveWeapon().Primary) then
      if (clip == -1) then return end
      
      local ammo_clip, ammo_max, ammo_inv = GetAmmo(client)
      
      surface.SetFont("open_sans_42b")
      local w, h = surface.GetTextSize("/ "..reserve)
      
      BlurShadowText(clip, "open_sans_64b", x - 25 - (w + 10) + config.x_ammo, y - 35 - h + config.y_ammo, colour.pure_white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_BOTTOM)
      BlurShadowText("/ "..reserve, "open_sans_42b", x - 25 + config.x_ammo, y - 18 - h + config.y_ammo, colour.white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_BOTTOM)
   end
end

local function DrawLicense()
   if (position < 20) then position = position + 0.1 end
   if (alpha != 255) then alpha = alpha + 2 end
  
   if (!config.license_over_preview) then
      surface.SetMaterial(icon_license)
      surface.SetDrawColor(255, 255, 255, alpha)
      surface.DrawTexturedRect(330, y - 107 - position + pos_y, 32, 32)
   end
end

local function DrawHunger(client)
   pHunger = (client:getDarkRPVar("Energy") or 0)
   Hunger = math.min(100, (Hunger == pHunger and Hunger) or Lerp(0.1, Hunger, pHunger))

   if (config.netheous_levelup or config.vrondakis_leveling_system) then experience_h_offset = 9 end

   local percentage = math.Round(GetPercentage(Hunger or 0, 100, 61 + experience_h_offset))
   if (percentage > 0) then offset = 1 end
  
   BorderedRect(370 + preview_w_offset, y - 111 + pos_y, 17, 101 + experience_h_offset, colour.dark, false, true, true, true, false, true)
   
   surface.SetTexture(texture_gradient)
   surface.SetDrawColor(Color(0, 0, 0, 150))
   surface.DrawTexturedRectRotated(377 + preview_w_offset, y - 60 + pos_y + ((experience_h_offset - 1) / 2), 17, 100 + (experience_h_offset - 1), 0)
   
   BorderedRect(374 + preview_w_offset, y - 85 + pos_y, 8, 64 + (experience_h_offset + 4), colour.light, true, true, true, true, true, true)
   NormalRect(375 + preview_w_offset, y - 24 - percentage + offset + pos_y + (experience_h_offset + 4), 6, percentage, GetPulse(colour.hunger, true, false, false, false, Hunger), true)

   surface.SetMaterial(icon_hunger)
   surface.SetDrawColor(255, 255, 255, 255)
   surface.DrawTexturedRect(373 + preview_w_offset, y - 100 + pos_y, 10, 10)
   
   if (pHunger <= 20) then
      local colour = Color(math.Clamp(math.sin(RealTime() * 10) * 225, 155, 225), 0, 0)
      
      BlurShadowText("!", "open_sans_25b", 376 + preview_w_offset, y - 111 + pos_y, colour, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
   end
end

local function DrawPanel()
   local experience_h_offset = 0
   
   if (config.netheous_levelup or config.vrondakis_leveling_system) then experience_h_offset = 9 end

   BorderedRect(10, y - 111 + pos_y, 360 + preview_w_offset, 101 + experience_h_offset, colour.darkest, true, true, true, true, true, true)
   NormalRect(10, y - 54 + pos_y, 360 + preview_w_offset, 51, Color(0, 0, 0, 60), false)
   NormalRect(11, y - 54 + pos_y, 358 + preview_w_offset, 1, Color(0, 0, 0, 10), false)

   if (config.preview_enabled) then
      BorderedRect(20, y - 101 + pos_y, 80, 80, colour.light, true, true, true, true, true, true)
   end
end

local function DrawWanted(client)
   if (client:getDarkRPVar("wanted")) then
      if (!config.preview_avatar) then
         render.SetScissorRect(21, y - 100 + pos_y, 99, y - 23 + pos_y, true)
            surface.SetTexture(texture_gradient)
            surface.SetDrawColor(Color(math.Clamp(math.sin(RealTime() * 10) * 255, 155, 255), 25, 25))
            surface.DrawTexturedRectRotated(69, y - 71 + pos_y, 140, 140, 225)
            
            surface.SetDrawColor(Color(25, 25, math.Clamp(math.sin(RealTime() * 20) * 225, 155, 225)))
            surface.DrawTexturedRectRotated(49, y - 51 + pos_y, 200, 200, 45)
         render.SetScissorRect(0, 0, 0, 0, false)
      end
         
      local text = "Jestes poszukiwany przez Policje!"
      local pulse = Color(math.Clamp(math.sin(RealTime() * 10) * 255, 195, 255), 0, 0)
      
      SimpleShadowText(text, "open_sans_25b", x / 2, 66, pulse, 150, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
   end
end

local function DrawInfo(client)
   pWallet = client:getDarkRPVar("money") or 0
   Wallet = (Wallet == pWallet and Wallet) or GetTransition(Wallet, pWallet, 5)
     
   salary = "Wyplata: "..GAMEMODE.Config.currency..FormatNumber(client:getDarkRPVar("salary") or 0)
   wallet = GAMEMODE.Config.currency..FormatNumber(Wallet)
   subheading = client:getDarkRPVar("job") or ""
   
   local name = client:Nick()
   local font = "open_sans_25b"
   
   if (string.len(name) + string.len(wallet) > 26) then
      name = string.sub(name, 1, 26 - string.len(wallet)).."..."
      font = "open_sans_19b"
   end
   
   if (config.show_usergroup) then subheading = GetGroup(client) end

   BlurShadowText(wallet, font, 359 + preview_w_offset, y - 90 + pos_y, colour.white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER)
   BlurShadowText(name, font, 110 + preview_w_offset, y - 90 + pos_y, colour.white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
   BlurShadowText(subheading, "tahoma_14", 110 + preview_w_offset, y - 68 + pos_y, colour.grey, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
   BlurShadowText(salary, "tahoma_14", 359 + preview_w_offset, y - 68 + pos_y, colour.grey, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER)
end

local function DrawVitals(client)
   if client:Health() > MaxHealth then MaxHealth = client:Health() end
   if client:Armor() > MaxArmour and MaxArmour <= 250 then MaxArmour = client:Armor() end
   
   if (!client:Alive()) then 
      Health = 0
      Armour = 0
      MaxHealth = 0 
      MaxArmour = 0 
   end

   Health = (Health == client:Health() and Health) or Lerp(0.1, Health, client:Health())
   Armour = (Armour == client:Armor() and Armour) or Lerp(0.1, Armour, client:Armor())
   
   local health_percentage = GetPercentage(Health or 0, MaxHealth + MaxArmour, 249)
   local armour_percentage = GetPercentage(Armour or 0, MaxHealth + MaxArmour, 249)
   
   local additional_bar_x = 235 + preview_w_offset
   local additional_bar_y = y - 40 + pos_y
   local additional_bar_h = 9
   local additional_bar_font = "tahoma_13b"
   local text_x_alignment = TEXT_ALIGN_CENTER
   
   if (config.stamina) then additional_bar_h = 0 end
   
   if (!config.stamina) then 
      additional_bar_y = y - 35 + pos_y
      additional_bar_font = "open_sans_19b" 
   end
   
   BorderedRect(110 + preview_w_offset, y - 47 + pos_y, 250, 17 + additional_bar_h, Color(62, 88, 99), true, true, true, true, true, true)
   
   if (client:Health() > 0) then 
      HealthArmourBar(111 + preview_w_offset, y - 46 + pos_y, 248, 14 + additional_bar_h, health_percentage, armour_percentage) 
   end
   
   if (client:Armor() > 0) then 
      additional_bar_x = 117 + preview_w_offset
      text_x_alignment = TEXT_ALIGN_LEFT 
   end
      
   BlurShadowText(string.Comma(client:Health()), additional_bar_font, additional_bar_x, additional_bar_y, colour.white, text_x_alignment, TEXT_ALIGN_CENTER)

   if (client:Armor() > 0) then
      BlurShadowText(string.Comma(client:Armor()), additional_bar_font, 352 + preview_w_offset, additional_bar_y, colour.white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER)
   end
end

local function DrawStamina(client)
   pStamina = (client:getDarkRPVar("tcb_Stamina") or 0)
   Stamina = math.min(100, (Stamina == pStamina and Stamina) or Lerp(0.1, Stamina, pStamina))
   local percentage = GetPercentage(Stamina or 0, 100, 249)
   
   BorderedRect(110 + preview_w_offset, y - 29 + pos_y, 250, 8, colour.bar, true, true, true, true, true, true)
   
   if (pStamina > 0) then
      NormalRect(111 + preview_w_offset, y - 28 + pos_y, percentage, 5, colour.stamina, true)
   end
end

local function DrawExperience(client)
   if (config.netheous_levelup) then
      pExperience = levelup.getExperience(client) or 0
      pMaxExperience = (levelup.getLevel(client) + 1) * levelup.config.expPerLevel or 0
   end
   
   if (config.vrondakis_leveling_system) then
      pExperience = (client:getDarkRPVar('xp') or 0)
      pMaxExperience = ((( 10 + (((client:getDarkRPVar('level') or 1) * ((client:getDarkRPVar('level') or 1) + 1) * 90)))) * LevelSystemConfiguration.XPMult)
   end

   Experience = math.min(pMaxExperience, (Experience == pExperience and Experience) or Lerp(0.1, Experience, pExperience))
   local percentage = GetPercentage(Experience or 0, pMaxExperience, 340 + preview_w_offset)

   BorderedRect(20, y - 16 + pos_y, 340 + preview_w_offset, 8, colour.bar, true, true, true, true, true, true)
   
   if (pExperience > 0) then
      NormalRect(21, y - 15 + pos_y, percentage, 5, colour.experience, true)
   end
end

local function DrawPreview(client)
   local model = client:GetModel()
   
   if (!config.preview_avatar) then
      if (!Preview or !ispanel(Preview)) then
         Preview = vgui.Create("DModelPanel")
         Preview:SetModel(model)
         Preview._Model = model
         Preview:SetPos(21, y - 100 + pos_y)
         Preview:SetSize(78, 77)
         Preview:SetAnimated(false)
         Preview:SetAmbientLight(colour.light)
         Preview._Ambience = colour.light
         Preview:SetCamPos(Vector(17, -5, 65.5))
         Preview:SetLookAt(Vector(-1, 0, 62.5))
         Preview:ParentToHUD()
         function Preview:LayoutEntity(Entity) return end
      end

      if (model != Preview._Model) then
         Preview:SetModel(model)
         Preview._Model = model
      end
      
      if (colour.light != Preview._Ambience) and (!client:getDarkRPVar("wanted")) then
         Preview:SetAmbientLight(colour.light)
      end
      
      if (client:getDarkRPVar("wanted")) then
         Preview:SetAmbientLight(colour.health)
      end
      
      render.SetScissorRect(21, y - 100 + pos_y, 99, y - 23 + pos_y, true)
         surface.SetTexture(texture_gradient)
         surface.SetDrawColor(colour.dark)
         surface.DrawTexturedRectRotated(49, y - 51 + pos_y, 200, 200, 45)
      render.SetScissorRect(0, 0, 0, 0, false)
   end
   
   if (config.preview_avatar) then
      if (!Preview or !ispanel(Preview)) then
         Preview = vgui.Create("AvatarImage")
         Preview:SetPos(21, y - 100 + pos_y)
         Preview:SetSize(78, 77)
         Preview:SetPlayer(client, 96)
         Preview:ParentToHUD()
      end 
   end
   
   Preview.PaintOver = function(self, w, h)
      if (config.netheous_levelup) then
         NormalRect(0, h - 15, w, 15, Color(0, 0, 0, 150), false)
         BlurShadowText("Level "..(levelup.getLevel(client) or 1), "tahoma_13", w / 2, h - 8, colour.pure_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      end
   
      if (config.vrondakis_leveling_system) then
         NormalRect(0, h - 15, w, 15, Color(0, 0, 0, 150), false)
         BlurShadowText("Level "..(client:getDarkRPVar("level") or 1), "tahoma_13", w / 2, h - 8, colour.pure_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      end
      
      if (config.preview_avatar) and (config.preview_wanted) and (client:getDarkRPVar("wanted")) then
         BlurShadowText("!", "open_sans_64b", w / 2, (h / 2) - 10, Color(math.Clamp(math.sin(RealTime() * 10) * 255, 195, 255), 0, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      end
      
      if (config.license_over_preview) then
         surface.SetMaterial(icon_license)
         surface.SetDrawColor(255, 255, 255, alpha)
         surface.DrawTexturedRect(w - 18, 3, 16, 16)
      end
   end
end

plyMeta.DrawPlayerInfo = plyMeta.DrawPlayerInfo or function(self)
	local pos = self:EyePos()

	pos.z = pos.z + config.entity_display_offset
	pos = pos:ToScreen()
   
	if (GAMEMODE.Config.showname) then
		local nick, plyTeam = self:Nick(), self:Team()
      
      if (config.entity_health_name) and (GAMEMODE.Config.showhealth) then
         nick = nick.." ("..self:Health()..")"
      end
      
		BlurShadowText(nick, "open_sans_25b", pos.x, pos.y, RPExtraTeams[plyTeam] and RPExtraTeams[plyTeam].color or team.GetColor(plyTeam), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
   
	if (GAMEMODE.Config.showhealth) and (!config.entity_health_name) then 
      if !table.HasValue(EntityText, "Health") then 
         table.insert(EntityText, "Health")
      end
   end
   
	if (GAMEMODE.Config.showjob) then
      if !table.HasValue(EntityText, "Job") then 
         table.insert(EntityText, "Job")
      end
   end
   
   if (config.organisation) then 
      if !table.HasValue(EntityText, "Organisation") then 
         table.insert(EntityText, "Organisation")
      end
   end
   
   if (config.netheous_levelup or config.vrondakis_leveling_system) and (config.entity_show_level) then 
      if !table.HasValue(EntityText, "Level") then 
         table.insert(EntityText, "Level")
      end
   end
  
   for k, v in pairs(EntityText) do
      local amount = 19
      
      if (k == 1) then amount = 20 end
   
      if (v == "Health") then 
         local health = DarkRP.getPhrase("health", self:Health())
         
         BlurShadowText(health, "open_sans_19b", pos.x, pos.y + (k * amount), colour.entity_health, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      end
   
      if (v == "Job") then
         local teamname = self:getDarkRPVar("job") or team.GetName(self:Team())
      
         BlurShadowText(teamname, "open_sans_19b", pos.x, pos.y + (k * amount), colour.entity_job, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      end
      
      if (v == "Organisation") then 
         local organisation = self:GetNWString("organization")
         
         BlurShadowText(organisation, "open_sans_19b", pos.x, pos.y + (k * amount), colour.entity_organisation, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      end
      
      if (v == "Level") then 
         local level = 1
         
         if (config.netheous_levelup) then
            level = "Level "..levelup:getLevel(self) or level
         end
         
         if (config.vrondakis_leveling_system) then
            level = "Level "..(self:getDarkRPVar("level") or level)
         end
         
         BlurShadowText(level, "open_sans_19b", pos.x, pos.y + (k * amount), colour.entity_level, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
      end
  end

	if (self:getDarkRPVar("HasGunlicense")) then
		surface.SetMaterial(icon_license)
		surface.SetDrawColor(255, 255, 255, 255)
      
      if (config.entity_license_above) then
         surface.DrawTexturedRect(pos.x - 8, pos.y - 26, 16, 16)
      else
         surface.SetFont("open_sans_25b")
         local text_w, text_h
         
         if (config.entity_health_name) then
            text_w, text_h = surface.GetTextSize(self:Nick().." ("..self:Health()..")")
         else
            text_w, text_h = surface.GetTextSize(self:Nick())
         end
         
         surface.DrawTexturedRect(pos.x + (text_w / 2) + 5, pos.y - 5, 16, 16)
      end
	end
end

plyMeta.DrawWantedInfo = plyMeta.DrawWantedInfo or function(self)
	if !self:Alive() then return end

	local pos = self:EyePos()
	if !pos:isInSight({client, self}) then return end

   pos.y = pos.y + 20
	pos.z = pos.z + 20
	pos = pos:ToScreen()

	local wantedText = DarkRP.getPhrase("wanted", tostring(self:getDarkRPVar("wantedReason")))
   local colour = Color(math.Clamp(math.sin(RealTime() * 10) * 255, 195, 255), 0, 0)

   draw.DrawNonParsedText(wantedText, "open_sans_19b", pos.x + 1, pos.y + 1, Color(0, 0, 0, 250), TEXT_ALIGN_CENTER)
   draw.DrawNonParsedText(wantedText, "open_sans_19b", pos.x + 2, pos.y + 2, Color(0, 0, 0, 180), TEXT_ALIGN_CENTER)
   draw.DrawNonParsedText(wantedText, "open_sans_19b", pos.x, pos.y, colour, TEXT_ALIGN_CENTER)
end

local function DrawEntityDisplay(client)
	local shootPos = client:GetShootPos()
	local aimVec = client:GetAimVector()

	for k, ply in pairs(players or player.GetAll()) do
		if (ply == client) or !ply:Alive() or ply:GetNoDraw() then continue end
		local hisPos = ply:GetShootPos()
		if (ply:getDarkRPVar("wanted")) then ply:DrawWantedInfo() end

		if (GAMEMODE.Config.globalshow) then
			ply:DrawPlayerInfo()
		elseif hisPos:DistToSqr(shootPos) < 400000 then
			local pos = hisPos - shootPos
			local unitPos = pos:GetNormalized()
         
			if unitPos:Dot(aimVec) > 0.90 then
				local trace = util.QuickTrace(shootPos, pos, client)
            
				if trace.Hit and trace.Entity ~= ply then return end
				ply:DrawPlayerInfo()
			end
		end
	end

	local trace = client:GetEyeTrace()

	if IsValid(trace.Entity) and trace.Entity:isKeysOwnable() and trace.Entity:GetPos():DistToSqr(client:GetPos()) < 40000 then
		trace.Entity:drawOwnableInfo()
	end
end

local function DrawHUD()
   local client = LocalPlayer()
   if !client:IsValid() or !IsValid(client) then return end

   if (config.netheous_levelup or config.vrondakis_leveling_system) and (pos_y != -8) then pos_y = -8 end
   if (config.preview_enabled) and (preview_w_offset != 0) then preview_w_offset = 0 end
   
   AdminTell()
   Arrested()
   DrawLockdown(client)
   DrawVoice(client)
   DrawAgenda(client)
   
   if (config.ammo) then DrawAmmo(client) end
   if (client:getDarkRPVar("HasGunlicense")) then DrawLicense() end
   if (config.hunger) then DrawHunger(client) end
    
   DrawPanel()
   DrawWanted(client)
   DrawInfo(client)
   DrawVitals(client)
   
   if (config.stamina) then DrawStamina(client) end  
   if (config.netheous_levelup or config.vrondakis_leveling_system) then DrawExperience(client) end
   if (config.preview_enabled) then DrawPreview(client) end
   
   DrawEntityDisplay(client)
end

function GAMEMODE:DrawDeathNotice(x, y)
	if (!GAMEMODE.Config.showdeaths) then return end
   
	self.BaseClass:DrawDeathNotice(x, y)
end

local function DisplayNotify(msg)
	local text = msg:ReadString()
   
	GAMEMODE:AddNotify(text, msg:ReadShort(), msg:ReadLong())
	surface.PlaySound("buttons/lightswitch2.wav")

	print(text)
end
usermessage.Hook("_Notify", DisplayNotify)

function GAMEMODE:HUDDrawPickupHistory()
   if (GAMEMODE.PickupHistory == nil) then return end

   local y = GAMEMODE.PickupHistoryTop
   local wide, tall = 0, 0

   for k, v in pairs(GAMEMODE.PickupHistory) do
      if v.time < CurTime() then
         if (v.y == nil) then v.y = y end
         
         v.x = x
         v.y = (v.y * 5 + y) / 6

         local delta = (v.time + v.holdtime) - CurTime()
         delta = delta / v.holdtime
         delta = math.Clamp(delta, 0, 1)

         v.name = FormatString(v.name)

         surface.SetFont("tahoma_13b")
          
         local w_name = surface.GetTextSize(v.name)
         local w_ammo = 0
         
         if v.amount then
            w_ammo = surface.GetTextSize(" ["..v.amount.."]")
            w_name = w_name + w_ammo
         end 
         
         local w_clamp, alpha = 0, 0

         if delta >= 0.85 then
            w_clamp = 0
         elseif delta < 0.85 and delta > 0.8 then
            w_clamp = math.Round(w_name * (1 - ((delta - 0.8) * 20)))
         elseif delta > 0.2 then
            w_clamp = w_name * 1
         elseif delta <= 0.2 and delta > 0.15 then
            w_clamp = math.Round(w_name * ((delta - 0.15) * 20))
         elseif delta <= 0.1 then
            w_clamp = 0
         end
         
         if delta > 0.9 then
            alpha = 1 - ((delta - 0.9) * 10)
         elseif delta < 0.1 then
            alpha = delta * 10
         else
            alpha = 1
         end
         
         local w = math.max(w_clamp, 0)

         if (config.pickup_background) then
            BorderedRect(x - 44 - w, v.y - 18, w + 27, 24, Color(0, 0, 0, 60), false, false, false, false, false, false)
         end
         
         render.SetScissorRect(x - 44 - w + 10, v.y - 18, x - 30, v.y + 6, true)
            BlurShadowText(v.name, "tahoma_13b", x - 30 - w_ammo, v.y - (v.height / 2), colour.white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER)

            if (v.amount) then
               BlurShadowText(" ["..v.amount.."]", "tahoma_13b", x - 30, v.y - (v.height / 2), colour.white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER)
            end
         render.SetScissorRect(x - 44 - w, v.y - 18, w + 10, 24, false) 

         y = y + (v.height + 16)
         wide = math.max(wide, v.width + v.height + 24)
         tall = tall + v.height + 18

         if (delta == 0) then GAMEMODE.PickupHistory[k] = nil end
      end
   end  

   GAMEMODE.PickupHistoryTop = (GAMEMODE.PickupHistoryTop * 5 + (ScrH() * 0.75 - tall) / 2) / 6
   GAMEMODE.PickupHistoryWide = (GAMEMODE.PickupHistoryWide * 5 + wide) / 6
end

function GAMEMODE:HUDDrawTargetID()
   return false
end

local hideHUDElements = {
   ["DarkRP_HUD"] = true,
   ["DarkRP_EntityDisplay"] = true,
   ["DarkRP_ZombieInfo"] = false,
   ["DarkRP_LocalPlayerHUD"] = true,
   ["DarkRP_Hungermod"] = true,
   ["DarkRP_Agenda"] = true,
   ["CHudHealth"] = true,
   ["CHudBattery"] = true,
   ["CHudAmmo"] = true,
   ["CHudSecondaryAmmo"] = true
}

hook.Add("HUDShouldDraw", "HideDefaultElements", function(name)
   if hideHUDElements[name] then return false end
end)

function GAMEMODE:HUDPaint()
	DrawHUD()
	self.BaseClass:HUDPaint()
end

print("Mantle HUD (DarkRP) version 1.1.1 loaded.")