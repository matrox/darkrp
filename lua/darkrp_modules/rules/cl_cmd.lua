-- don't touch this
local rules
--

-- changeable variables
local w = ScrW() / 1.25
local h = ScrH() / 1.25
local site = "http://gamescitadel.eu/forum/showthread.php?tid=9"
--

function showRules(ply, cmd, args)
		rules = vgui.Create("DFrame")
		rules:SetSize(w, h)
		rules:SetPos(ScrW() / 2 - (w / 2), ScrH() / 2 - (h / 2))
		rules:SetTitle("Rules")
		rules:SetDraggable(true)
		rules:ShowCloseButton(false)
		rules:SetDeleteOnClose(false)
		rules:SetBackgroundBlur(true)
		rules:MakePopup()
		rules.Paint = function()
			-- top bar
			surface.SetDrawColor(60, 60, 60, 255)
			surface.DrawRect(0, 0, rules:GetWide(), rules:GetTall())
			
			-- actual shop area
			surface.SetDrawColor(40, 40, 40, 255)
			surface.DrawRect(0, 24, rules:GetWide(), rules:GetTall() - 24)
		end
		
		local closebutton = vgui.Create("DButton")
		closebutton:SetParent(rules)
		closebutton:SetText("")
		closebutton:SetSize(24, 24)
		closebutton:SetPos(rules:GetWide()-24, 0)
		closebutton.DoClick = function()
			if (rules:IsVisible()) then
				rules:Remove()
				gui.EnableScreenClicker(false)
				return
			end
		end
		closebutton.Paint = function()
			--Color of entire button
			surface.SetDrawColor(50, 50, 50, 255)
			surface.DrawRect(0, 0, closebutton:GetWide(), closebutton:GetTall())
			
			--Draw left border
			surface.SetDrawColor(40, 40, 40, 255)
			surface.DrawRect(0, 0, 1, closebutton:GetTall())
			
			-- Draw text
			draw.DrawText("X", "DermaDefaultBold", closebutton:GetWide() / 2, 5, Color(255, 255, 255, 255), 1)
		end
		
		local rules = vgui.Create("HTML", rules)
		rules:SetPos(0, 24)
		rules:SetSize(rules:GetWide(), rules:GetTall() - 24)
		rules:Dock( FILL )
		rules:OpenURL(site)
		
		gui.EnableScreenClicker(true)
end
concommand.Add("show_rules", showRules)