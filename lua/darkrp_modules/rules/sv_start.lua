function JoinRules(ply)
	ply:ConCommand("show_rules");
end
hook.Add("PlayerInitialSpawn", "JoinRules", JoinRules)

function ChatRules(ply, text, public)
	local lowered = string.lower(text)
	
	if (lowered == "/rules" or lowered == "!rules") then
		ply:ConCommand("show_rules")
	end
end
hook.Add("PlayerSay", "ChatRules", ChatRules)