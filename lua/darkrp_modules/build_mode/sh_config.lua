buildMode_Config = {
	["buildMode_Weapons"] = {"weapon_physgun", "gmod_tool", "keys"}, //The weapons that are distributed to players who enter build mode (They are only allowed to use doors).
	["buildMode_Songs"] = { //SoundCloud URLs that are played during build mode.
},
    ["buildMode_Delay"] = 120, //If a player recently exited build mode, this is how much time (in seconds) they have to wait before being able to use it again.
}