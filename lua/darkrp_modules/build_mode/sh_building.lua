if (SERVER) then
	util.AddNetworkString("buildMode_PlayMusic")
	
    DarkRP.defineChatCommand("build", function(ply)
        if (ply:GetNWInt("buildingPrevented") > 0) then
		    DarkRP.notify(ply, 1, 5, "You need wait more "..ply:GetNWInt("buildingPrevented").." seconds before using Build Mode.")
		elseif (ply:GetNWBool("isBuilding")) then 
		    ply:GodDisable() -- Sure he can't be shot while invisible, but we don't want any player inside build mode dying from falling, do we?
		    ply:StripWeapons()
		    ply:SetNWBool("isBuilding", false)
			ply:SetNWInt("buildingPrevented", buildMode_Config["buildMode_Delay"])
		    ply:SetCustomCollisionCheck(false)	
            
		    for k, wep in pairs(ply.restoreWeapons) do
				ply:Give(wep)
			end
			
            ply.restoreWeapons = nil
			DarkRP.notify(ply, 0, 5, "You are now out of Build Mode, several features like Voice Communication have been enabled again.")
		else	
			ply.restoreWeapons = {}
		        
			for k, wep in pairs(ply:GetWeapons()) do
				table.insert(ply.restoreWeapons, wep:GetClass())
			end
				
			ply:GodEnable()
			ply:StripWeapons()
			ply:SetNWBool("isBuilding", true)
			ply:SetCustomCollisionCheck(true)
			
	        for k, wep in pairs(buildMode_Config["buildMode_Weapons"]) do
	            ply:Give(wep)
	        end
			 
			net.Start("buildMode_PlayMusic")
			net.Send(ply)
				
			DarkRP.notify(ply, 0, 5, "You are now in Build Mode, several features like Voice Communication have been disabled.")
		end   
	
	    return ""
	end)
	
	timer.Create("buildMode_NoAbusing", 1, 0, function() //Can be optimized, will do that soon...
		for k, v in pairs(player.GetAll()) do
		    if (v:GetNWInt("buildingPrevented") > 0) then
			    v:SetNWInt("buildingPrevented", v:GetNWInt("buildingPrevented") -1)
			end
        end
	end)
	
	hook.Add("PlayerCanHearPlayersVoice", "buildMode_Silence", function(listen, talk)
	    if (listen:GetNWBool("isBuilding") or talk:GetNWBool("isBuilding")) then
		    return false
		end
	end)
	
	hook.Add("PlayerUse", "buildMode_Usage", function(ply, ent)
	    if (ply:GetNWBool("isBuilding")) then
		    if (ent:GetClass() == "prop_door_rotating" or ent:GetClass() == "func_door_rotating") then
		        return true
			else
				DarkRP.notify(ply, 1, 5, "You can only use doors during Build Mode.")
			    return false
			end
		end
	end)
	
	hook.Add("CanPlayerSuicide", "buildMode_Suicide", function(ply)
	    if (ply:GetNWBool("isBuilding")) then
		    DarkRP.notify(ply, 1, 5, "Suiciding is disabled during Build Mode.")
			return false
		end
	end)
	
	hook.Add("playerCanChangeTeam", "buildMode_TeamDisable", function(ply)
	    if (ply:GetNWBool("isBuilding")) then
			return false, "Team changing is disabled during Build Mode."
		end
	end)
else
	hook.Add("Think", "buildMode_Transparency", function() //I will most likely change how this works in the future, I just don't like the way It's done right now and this is horribly unoptimized...
		if (LocalPlayer():GetNWBool("isBuilding")) then
			for k, v in pairs(player.GetAll()) do 
                if (LocalPlayer() != v) then
				    v:SetColor(Color(255, 255, 255, 100))
			  	    v:SetRenderMode(RENDERMODE_TRANSALPHA)
				end
		    end
				
		    for k, v in pairs(ents.FindByClass("prop_physics")) do
		        if (v.Owner:IsPlayer() and v.Owner != LocalPlayer()) then
			        v:SetColor(Color(255, 255, 255, 100))
				    v:SetRenderMode(RENDERMODE_TRANSALPHA)
			    end
		    end
		else
			for k, v in pairs(player.GetAll()) do 
			    if (v:GetNWBool("isBuilding")) then
				    v:SetColor(Color(255, 255, 255, 100))
			  	    v:SetRenderMode(RENDERMODE_TRANSALPHA)
				else
				    v:SetColor(Color(255, 255, 255))
				    v:SetRenderMode(RENDERMODE_NORMAL)
				end
		    end
				
		    for k, v in pairs(ents.FindByClass("prop_physics")) do 
			    if (v.Owner:GetNWBool("isBuilding")) then
				    v:SetColor(Color(255, 255, 255, 100))
			  	    v:SetRenderMode(RENDERMODE_TRANSALPHA)
			    else
				    v:SetColor(Color(255, 255, 255))
				    v:SetRenderMode(RENDERMODE_NORMAL)
				end
		    end
	    end
	end)
	
	hook.Add("PostPlayerDraw", "buildMode_HeadText", function(ply)
	    if (ply:GetNWBool("isBuilding") and LocalPlayer():GetPos():Distance(ply:GetPos()) < 1000 and LocalPlayer() != ply) then
            local ang = LocalPlayer():EyeAngles()
		    local pos = ply:GetPos() + ang:Up()

		    ang:RotateAroundAxis(ang:Forward(), 90)
		    ang:RotateAroundAxis(ang:Right(), 90)
		
		    cam.Start3D2D(pos, Angle(0, ang.y, 90), 0.15)
			    draw.SimpleTextOutlined("Build Mode", "Trebuchet24", 0, -508, Color(255, 255, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(0, 0, 0)) 
                draw.SimpleText("I'm OFFRP, don't disturb!", "Trebuchet18", 0, -490, Color(0, 0, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		    cam.End3D2D()
		end
	end)
	
	hook.Add("HUDPaint", "buildMode_HUDText", function() //I suck at making HUDs, fuck this resolution shit man... This is good enough, forgive me if anything is wrong (most likely is).
	    if (LocalPlayer():GetNWBool("isBuilding")) then
		    draw.SimpleText("Build Mode", "Trebuchet24", ScrW() /2, 15, Color(150, 20, 20), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
            draw.SimpleText("You are OFFRP and no one should disturb you, some features have been disabled.", "Trebuchet18", ScrW() /2, 31, Color(0, 0, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	    end
	end)
end

hook.Add("ShouldCollide", "buildMode_NoCollide", function(ent1, ent2)
	if (ent1:GetNWBool("isBuilding") and ent2:IsPlayer()) then
	    return false
	end
end)

DarkRP.declareChatCommand{
	command = "build",
	description = "Enter Build Mode and avoid being disturbed during building!",
	delay = 1.5 //What an amazing idea, a delay that simply makes the command not work... Not even a single one second notification, just straight up not working... (ノ°□°)ノ︵ ┻━┻
}