local function addLazyNotify(enum, len, msg) //100% unneeded, but we know who's the mother of all inventions!
    notification.AddLegacy(msg, enum, len)
	surface.PlaySound("buttons/lightswitch2.wav")
end

function buildMode_PlayURL()
	http.Fetch("http://api.soundcloud.com/resolve.json?url="..table.Random(buildMode_Config["buildMode_Songs"]).."&client_id=f735c28b3f75bd3336ec7ccb0cbbc7a8", //Please don't use my ClientID for your programming needs, get one for yourself at SoundCloud.
	    function(json)
		    local tab = util.JSONToTable(json)
			
			if (tab["errors"]) then
		        addLazyNotify(1, 3, "Couldn't play unidentified music! ("..tab["errors"][1]["error_message"]..")")
		    elseif (not tab["streamable"]) then
			    addLazyNotify(1, 3, "Couldn't play non-streamable music:" ..tab["title"])
		    else
			    addLazyNotify(0, 3, "Now loading: "..tab["title"])
				
				sound.PlayURL(tab["stream_url"].."?client_id=f735c28b3f75bd3336ec7ccb0cbbc7a8", "", function(music) //Please don't use my ClientID for your programming needs, get one for yourself at SoundCloud.
		            if (music) then
			            LocalPlayer().playingMusic = music
			            LocalPlayer().playingMusic:Play()
			
			            hook.Add("Think", "buildMode_StopURL", function() 
			                if (not LocalPlayer():GetNWBool("isBuilding")) then 
				                LocalPlayer().playingMusic:Stop() 
					
					            timer.Remove("buildMode_Replay")
					            hook.Remove("Think", "buildMode_StopURL") 
				            end
			            end)
			
			            timer.Create("buildMode_Replay", tab["duration"] /1000, 1, function() buildMode_PlayURL() end) //I was going to use the more resource intensive flag that enables me to get the audio length and all of that, but this works almost as perfectly. I might change if people start reporting problems...
						
			            addLazyNotify(0, 5, "Now playing: "..tab["title"])
		            else
                        addLazyNotify(1, 3, "Failed to load: "..tab["title"].." (timeout?)")
					end
				end)
			end
		end,
		function(error)
		    addLazyNotify(1, 3, "Couldn't play unidentified music! ("..error..")")
		end
	)
end
net.Receive("buildMode_PlayMusic", buildMode_PlayURL)