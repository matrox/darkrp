--[[---------------------------------------------------------------------------
DarkRP custom shipments and guns
---------------------------------------------------------------------------

This file contains your custom shipments and guns.
This file should also contain shipments and guns from DarkRP that you edited.

Note: If you want to edit a default DarkRP shipment, first disable it in darkrp_config/disabled_defaults.lua
	Once you've done that, copy and paste the shipment to this file and edit it.

The default shipments and guns can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/addentities.lua

For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomShipmentFields


Add shipments and guns under the following line:
---------------------------------------------------------------------------]]

-- Drugz
DarkRP.createShipment("Papierosy", {
        model = "models/boxopencigshib.mdl",
        entity = "durgz_cigarette",
        price = 1000,
        amount = 10,
        separate = false,
        pricesep = 0,
        noship = false,
        allowed = {TEAM_DRUGS}
})

-- CS:S Weps
DarkRP.createShipment("MP5K", {
   model = "models/weapons/w_mp5.mdl",
   entity = "fas2_mp5k",
   price = 6000,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})

DarkRP.createShipment("Deagle", {
   model = "models/weapons/w_pist_deagle.mdl",
   entity = "fas2_deagle",
   price = 6000,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})

 
DarkRP.createShipment("glock", {
   model = "models/weapons/w_pist_glock18.mdl",
   entity = "fas2_glock20",
   price = 4000,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})

DarkRP.createShipment("M67", {
   model = "models/weapons/w_eq_fraggrenade_thrown.mdl",
   entity = "fas2_m67",
   price = 4000,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})
 
DarkRP.createShipment("AK47", {
   model = "models/weapons/w_ak47.mdl",
   entity = "fas2_ak47",
   price = 4000,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS}
})
 
DarkRP.createShipment("M4A1", {
   model = "models/weapons/w_rif_m4a1.mdl",
   entity = "fas2_m4a1",
   price = 3900,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS}
})
 
 DarkRP.createShipment("Remington 870", {
   model = "models/weapons/w_shot_m3super90.mdl",
   entity = "fas2_rem870",
   price = 3900,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS}
})
 
 DarkRP.createShipment("MAC10", {
   model = "models/weapons/w_smg_mac10.mdl",
   entity = "fas2_mac11",
   price = 3900,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS}
})
 
  DarkRP.createShipment("Kajdanki", {
   model = "models/weapons/spy/w_handcuffs.mdl",
   entity = "darkrp_handcuffs",
   price = 1400,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_SZARY}
})

  DarkRP.createShipment("Flamethrower", {
   model = "models/weapons/w_ai_flamethrower.mdl",
   entity = "weapon_ai_flamethrower",
   price = 1400,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_FLTH}
})
 
 DarkRP.createShipment("Lockpick", {
   model = "models/weapons/w_crowbar.mdl",
   entity = "lockpick",
   price = 1000,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_SZARY}
})

 DarkRP.createShipment("Unarrest Stick", {
   model = "models/weapons/w_stunbaton.mdl",
   entity = "unarrest_stick",
   price = 1500,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_SZARY}
})

 DarkRP.createShipment("Keypad Cracker", {
   model = "models/weapons/w_c4.mdl",
   entity = "keypad_cracker",
   price = 2000,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_SZARY}
})

 DarkRP.createShipment("Granat", {
   model = "models/weapons/w_eq_fraggrenade.mdl",
   entity = "fas2_m67",
   price = 2000,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})

 DarkRP.createShipment("Maczeta", {
   model = "models/weapons/w_machete.mdl",
   entity = "fas2_machete",
   price = 2000,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})

 DarkRP.createShipment("P226", {
   model = "models/weapons/w_pist_p228.mdl",
   entity = "fas2_p226",
   price = 1500,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})

 DarkRP.createShipment("OTS33", {
   model = "models/weapons/world/pistols/ots33.mdl",
   entity = "fas2_ots33",
   price = 1520,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNSL}
})

 DarkRP.createShipment("Tlumik", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_suppressor",
   price = 3520,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("Acog 4x", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_acog",
   price = 1520,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("Comp M4", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_compm4",
   price = 1540,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("Elcan C79", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_c79",
   price = 1520,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("EOTech 553", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_eotech",
   price = 1530,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("Foregrip", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_foregrip",
   price = 1530,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("Harris Bipod", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_harrisbipod",
   price = 1540,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("Leupold mk4", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_leupold",
   price = 1550,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("M21 20RND Mag", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_m2120mag",
   price = 1560,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("SG 556X 30RND Mag", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_sks30mag",
   price = 1570,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("PSO 1", {
   model = "models/Items/BoxSRounds.mdl",
   entity = "fas2_att_pso1",
   price = 1570,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS},
   category = "Dodatki do fas 2.0",
})

 DarkRP.createShipment("1911", {
   model = "models/weapons/w_1911.mdl",
   entity = "fas2_m1911",
   price = 1600,
   amount = 5,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_GUNS}
})



DarkRP.createShipment("G3a3", {
	model = "models/weapons/w_g3a3.mdl",
	entity = "fas2_g3",
	price = 5800,
	amount = 5,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNS}
})


DarkRP.createShipment("M24", {
	model = "models/weapons/w_snip_awp.mdl",
	entity = "fas2_m24",
	price = 12000,
	amount = 3,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNS}
})


DarkRP.createShipment("MP5a5", {
	model = "models/weapons/w_smg_mp5.mdl",
	entity = "fas2_mp5a5",
	price = 1800,
	amount = 5,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNS}
})


DarkRP.createShipment("PP-19 Bizon", {
	model = "models/weapons/w_smg_biz.mdl",
	entity = "fas2_pp19",
	price = 5800,
	amount = 5,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNS}
})

DarkRP.createShipment("Raging Bull ", {
	model = "models/weapons/w_357.mdl",
	entity = "fas2_ragingbull",
	price = 1800,
	amount = 5,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNSL}
})



DarkRP.createShipment("SG-552 Commando ", {
	model = "models/weapons/w_sg550.mdl",
	entity = "fas2_sg552",
	price = 12500,
	amount = 3,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNS}
})



DarkRP.createShipment("SKS Carbine ", {
	model = "models/weapons/w_snip_awp.mdl",
	entity = "fas2_sks",
	price = 3800,
	amount = 3,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNS}
})


DarkRP.createShipment("UZI", {
	model = "models/weapons/w_smg_mp5.mdl",
	entity = "fas2_uzi",
	price = 4800,
	amount = 5,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_GUNS}
})



--Fasy na dole other

DarkRP.createShipment("Fire Extinguisher ", {
	model = "models/weapons/w_fire_extinguisher.mdl",
	entity = "weapon_extinguisher",
	price = 100,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_FIREMAN}
})


DarkRP.createShipment("Piwko", {
	model = "models/drug_mod/alcohol_can.mdl",
	entity = "durgz_alcohol",
	price = 130,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_BARMAN}
})

DarkRP.createShipment("Aspiryna", {
	model = "models/jaanus/aspbtl.mdl",
	entity = "durgz_aspirin",
	price = 230,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})

DarkRP.createShipment("Cygareta", {
	model = "models/boxopencigshib.mdl",
	entity = "durgz_cigarette",
	price = 330,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_BARMAN}
})

DarkRP.createShipment("Kokaina", {
	model = "models/cocn.mdl",
	entity = "durgz_cocaine",
	price = 430,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})

DarkRP.createShipment("Heroina", {
	model = "models/katharsmodels/syringe_out/syringe_out.mdl",
	entity = "durgz_heroine",
	price = 530,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})
DarkRP.createShipment("Apteczka", {
	model = "models/Items/HealthKit.mdl",
	entity = "fas2_ifak",
	price = 530,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_MEDYK}
})

DarkRP.createShipment("LSD", {
	model = "models/smile/smile.mdl",
	entity = "durgz_lsd",
	price = 630,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})

DarkRP.createShipment("Meta", {
	model = "models/katharsmodels/contraband/metasync/blue_sky.mdl",
	entity = "durgz_meth",
	price = 730,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})

DarkRP.createShipment("Grzyby", {
	model = "models/ipha/mushroom_small.mdl",
	entity = "durgz_mushroom",
	price = 830,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})

DarkRP.createShipment("PCP", {
	model = "models/marioragdoll/Super Mario Galaxy/star/star.mdl",
	entity = "durgz_pcp",
	price = 930,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})

DarkRP.createShipment("Woda", {
	model = "models/drug_mod/the_bottle_of_water.mdl",
	entity = "durgz_water",
	price = 0,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_BARMAN}
})

DarkRP.createShipment("Ziola", {
	model = "models/katharsmodels/contraband/zak_wiet/zak_wiet.mdl",
	entity = "durgz_weed",
	price = 1000,
	amount = 1,
	seperate = false,
	pricesep = nil,
	noship = false,
	allowed = {TEAM_DRUGS}
})

 DarkRP.createShipment("Popcorn", {
   model = "models/Teh_Maestro/popcorn.mdl",
   entity = "weapon_popcorn",
   price = 200,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_CINEMASALLER}
})
 DarkRP.createShipment("Hacker ATM", {
   model = "models/props_lab/reciever01d.mdl",
   entity = "weapon_arc_atmhack",
   price = 500,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   allowed = {TEAM_HACKER}
})
 DarkRP.createShipment("CS GO KNIFE 1", {
   model = "models/weapons/w_knife_t.mdl",
   entity = "clt_karamcse",
   price = 500,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta bron jest tylko dla VIP!",
})
 DarkRP.createShipment("CS GO KNIFE 2", {
   model = "models/weapons/w_knife_t.mdl",
   entity = "clt_karamfde",
   price = 1000,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta bron jest tylko dla VIP!",
})
 DarkRP.createShipment("CS GO KNIFE 3", {
   model = "models/weapons/w_knife_t.mdl",
   entity = "clt_karamstk",
   price = 5000,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta bron jest tylko dla VIP!",
})
 DarkRP.createShipment("CS GO KNIFE 4", {
   model = "models/weapons/w_knife_t.mdl",
   entity = "clt_karamtgt",
   price = 10000,
   amount = 1,
   separate = false,
   pricesep = 0,
   noship = false,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta bron jest tylko dla VIP!",
})