--[[---------------------------------------------------------------------------
DarkRP custom entities
---------------------------------------------------------------------------

This file contains your custom entities.
This file should also contain entities from DarkRP that you edited.

Note: If you want to edit a default DarkRP entity, first disable it in darkrp_config/disabled_defaults.lua
	Once you've done that, copy and paste the entity to this file and edit it.

The default entities can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/addentities.lua#L111

For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomEntityFields

Add entities under the following line:
---------------------------------------------------------------------------]]

DarkRP.createEntity( "Telewizor", 
{
	ent = "mediaplayer_tv",
	model = "models/gmod_tower/suitetv_large.mdl",
	price = 500,
	max = 2,
	allowed = {TEAM_CINEMAOWNER},
	category = "Media Player",
	cmd = "buymediaplayer",
} )

DarkRP.createEntity( "Terminal", 
{
	ent = "sent_computer",
	model = "models/props_lab/monitor01a.mdl",
	price = 1500,
	max = 2,
	category = "PC",
	cmd = "buycomputer",
} )

DarkRP.createEntity( "Paleta Kasy", 
{
	ent = "bank_vault",
	model = "models/props/cs_assault/moneypallet.mdl",
	price = 2500,
	max = 2,
	allowed = {TEAM_BANKMANAGER},
	category = "Money",
	cmd = "buymonpallete",
} )

DarkRP.createEntity( "Mikrofon", 
{
	ent = "radio_microphone",
	model = "models/mic.mdl",
	price = 250,
	max = 1,
	allowed = {TEAM_OPRADIO},
	category = "Radio",
	cmd = "buymikro",
} )
DarkRP.createEntity( "Drum Pad", 
{
	ent = "instrument_drumpad",
	model = "models/metasync/gpad.mdl",
	price = 250,
	max = 1,
	allowed = {TEAM_PERKUSIA},
	category = "Muzyka",
	cmd = "buyperkusja",
} )
DarkRP.createEntity( "BoomBOX", 
{
	ent = "3d_boombox",
	model = "models/boomboxv2/boomboxv2.mdl",
	price = 1550,
	max = 3,
	allowed = {TEAM_DJ},
	category = "Muzyka",
	cmd = "buyboombox",
} )


DarkRP.createEntity( "Drum", 
{
	ent = "instrument_drum",
	model = "models/yukitheater/drums.mdl",
	price = 250,
	max = 1,
	allowed = {TEAM_PERKUSIA},
	category = "Muzyka",
	cmd = "buyperkusja1",
} ) 
DarkRP.createEntity( "Radio - Muzyka", 
{
	ent = "wf_radio",
	model = "models/props_marines/army_radio.mdl",
	price = 250,
	max = 1,
	category = "Radio",
	cmd = "buyradiomuz",
} )

DarkRP.createEntity( "Radio", 
{
	ent = "radio_receiver",
	model = "models/props_lab/citizenradio.mdl",
	price = 150,
	max = 2,
	category = "Radio",
	cmd = "buyradio",
} )

DarkRP.createEntity( "Pianino", { 
   ent = "gmt_instrument_piano",
   model = "models/fishy/furniture/piano.mdl",
   price = 2500,
   max = 1,
   cmd = "piano",
   category = "Muzyka",
   -- CustomCheck
})

DarkRP.createEntity("Kanister z Gazem", {
	ent = "eml_gas",
	model = "models/props_c17/canister01a.mdl",
	price = 100,
	max = 20,
	allowed = {TEAM_METHA},
    category = "Kucharz Mety",
	cmd = "buygascanister"
})

DarkRP.createEntity("Rozwodniona Jodyna", {
	ent = "eml_iodine",
	model = "models/props_lab/jar01b.mdl",
	price = 500,
	max = 20,
	category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buyiodine"
})

DarkRP.createEntity("Sloik", {
	ent = "eml_jar",
	model = "models/props_lab/jar01a.mdl",
	price = 300,
	max = 20,
    category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buyjar"
})

DarkRP.createEntity("Kwas", {
	ent = "eml_macid",
	model = "models/props_junk/garbage_plasticbottle001a.mdl",
	price = 200,
	max = 20,
    category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buymacid"
})

DarkRP.createEntity("Garnek", {
	ent = "eml_pot",
	model = "models/props_c17/metalPot001a.mdl",
	price = 100,
	max = 20,
    category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buypot"
})

DarkRP.createEntity("Garnek do Mety", {
	ent = "eml_spot",
	model = "models/props_c17/metalPot001a.mdl",
	price = 50,
	max = 20,
    category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buyspot"
})

DarkRP.createEntity("Kuchenka Gazowa", {
	ent = "eml_stove",
	model = "models/props_c17/furnitureStove001a.mdl",
	price = 100,
	max = 5,
    category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buystove"
})

DarkRP.createEntity("Laboratorium Narkotykow", {
    ent = "drug_lab",
    model = "models/props_lab/crematorcase.mdl",
    price = 400,
    max = 3,
    cmd = "druglab",
    allowed = {TEAM_METHA},
})

DarkRP.createEntity("Laboratorium Broni", {
    ent = "gunlab",
    model = "models/props_c17/TrapPropeller_Engine.mdl",
    price = 500,
    max = 1,
    cmd = "kupbonlab",
    allowed = {TEAM_GUNS, TEAM_GUNSL},
})

DarkRP.createEntity("Rozwodniona Siarka", {
	ent = "eml_sulfur",
	model = "models/props_lab/jar01b.mdl",
	price = 100,
	max = 20,
    category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buysulfur"
})



DarkRP.createEntity("Woda", {
	ent = "eml_water",
	model = "models/props_junk/garbage_plasticbottle003a.mdl",
	price = 100,
	max = 20,
    category = "Kucharz Mety",
	allowed = {TEAM_METHA},
	cmd = "buywater"
})

DarkRP.createEntity("Terminal Sprzedawcy", {
	ent = "sent_arc_pinmachine",
	model = "models/arc/atm_cardmachine.mdl",
	price = 1000,
	max = 20,
    category = "Money",
	cmd = "buyterminal"
})

DarkRP.createEntity("Jednoreki bandyta", {
	ent = "slot_machine",
	model = "models/props/slotmachine/slotmachinefinal.mdl",
	price = 1000,
	max = 20,
    category = "Money",
	cmd = "buybandita",
	allowed = {TEAM_JCAGE},
})
DarkRP.createEntity("Pacman", {
	ent = "pacman",
	model = "models/arcadefun/pacman.mdl",
	price = 1000,
	max = 20,
    category = "Gamers",
	cmd = "buypcman",
	allowed = {TEAM_GAMERBOSS},
})
DarkRP.createEntity("Flamethrower Ammo", {
	ent = "item_ai_propane_tank",
	model = "models/weapons/item_ai_flamethrower_tank.mdl",
	price = 1000,
	max = 20,
	cmd = "buyflammo",
	allowed = {TEAM_FLTH},
})
DarkRP.createEntity("Donkeykong", {
	ent = "donkeykong",
	model = "models/arcadefun/donkykong.mdl",
	price = 1000,
	max = 20,
    category = "Gamers",
	cmd = "buydonkeykong",
	allowed = {TEAM_GAMERBOSS},
})
DarkRP.createEntity("Drukarka", {
        ent = "adv_moneyprinter",
        model = "models/props_c17/consolebox01a.mdl",
        price = 2500,
        max = 5,
        category = "Printerki",
        cmd = "buyprinter23",
})
DarkRP.createEntity("Chlodzenie Drukarki", {
        ent = "printer_cooler",
        model = "models/props_c17/consolebox01a.mdl",
        price = 1000,
        max = 5,
		category = "Printerki",
        cmd = "buycooler23",
})
DarkRP.createEntity("Zabezpieczenie Drukarki", {
        ent = "printer_failsafe",
        model = "models/props_c17/consolebox01a.mdl",
        price = 500,
        max = 5,
		category = "Printerki",
        cmd = "buyfailsafe23",
})
DarkRP.createEntity("Overclock Drukarki", {
        ent = "printer_overclocker",
        model = "models/props_c17/consolebox01a.mdl",
        price = 1000,
        max = 5,
		category = "Printerki",
        cmd = "buyoverclocker23",
})
AddEntity("Mala Bateria", {
        ent = "printer_smallbattery",
        model = "models/props_c17/consolebox05a.mdl",
        price = 200,
        max = 10,
		category = "Printerki",
        cmd = "buysbattery23",
})
AddEntity("Srednia Bateria", {
        ent = "printer_mediumbattery",
        model = "models/props_c17/consolebox05a.mdl",
        price = 600,
        max = 10,
		category = "Printerki",
        cmd = "buymbattery23",
})
AddEntity("Wielka Bateria", {
        ent = "printer_largebattery",
        model = "models/props_c17/consolebox05a.mdl",
        price = 1500,
        max = 10,
		category = "Printerki",
        cmd = "buylbattery23",
})
DarkRP.createEntity("Stacja z HP", { 
ent = "health_station", 
model = "models/props_c17/consolebox01a.mdl", 
price = 5500, 
max = 1, 
cmd = "health_station", 
}) 
DarkRP.createEntity("Stacja z Pancerzem", { 
ent = "armour_station", 
model = "models/props_c17/consolebox01a.mdl", 
price = 2500, 
max = 1, 
cmd = "armour_station", 
}) 
DarkRP.createEntity("Alarm do drzwi (ALPHA)", {
	ent = "door_alarm",
	model = "models/props_lab/reciever01d.mdl",
	price = 200,
	max = 5,
	cmd = "buyalarm"
})
DarkRP.createEntity("Skrzynka pod rude", {
	ent = "eoti_mining_crate",
	model = "models/crate/miningcrate.mdl",
	price = 50,
	max = 5,
	cmd = "kupminingcrate",
	allowed = {TEAM_GORNIK}
})
