--[[---------------------------------------------------------------------------
Group chats
---------------------------------------------------------------------------
Team chat for when you have a certain job.
e.g. with the default police group chat, police officers, chiefs and mayors can
talk to one another through /g or team chat.

HOW TO MAKE A GROUP CHAT:
Simple method:
GAMEMODE:AddGroupChat(List of team variables separated by comma)

Advanced method:
GAMEMODE:AddGroupChat(a function with ply as argument that returns whether a random player is in one chat group)
This is for people who know how to script Lua.

---------------------------------------------------------------------------]]

-- Example: GAMEMODE:AddGroupChat(TEAM_MOB, TEAM_GANG)
-- Example: GAMEMODE:AddGroupChat(function(ply) return ply:isCP() end)
GAMEMODE:AddGroupChat(TEAM_PSY, TEAM_SZEFP, TEAM_BUREK, TEAM_FIREMAN, TEAM_OCHWIEZIENIA, TEAM_DWIEZIENIA, TEAM_SEDZIA, TEAM_PRAWNIK, TEAM_PRO, TEAM_SNAJPER, TEAM_SWAT, TEAM_KOMORNIK, TEAM_SEKRETARKA, TEAM_SAPER)
GAMEMODE:AddGroupChat(TEAM_CAPO, TEAM_MAFIA)
GAMEMODE:AddGroupChat(TEAM_MOB, TEAM_ZABOJCZYNI)
GAMEMODE:AddGroupChat(TEAM_ADMIN, TEAM_SADMIN, TEAM_MODERATOR)
GAMEMODE:AddGroupChat(TEAM_BANK, TEAM_BANKMANAGER)
GAMEMODE:AddGroupChat(TEAM_CINEMAOWNER, TEAM_CINEMASALLER)
GAMEMODE:AddGroupChat(TEAM_GAMERBOSS, TEAM_GAMERSELLER)