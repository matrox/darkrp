--[[
Stare prace DarkRP 
Zrobilismy je kiedys razem
--]]
-- Administracyjna Praca
TEAM_ADMIN = DarkRP.createJob("Admin Na Sluzbie", {
   color = Color(180, 0, 0, 255),
   model = {"models/player/swat.mdl"},
   description = [[Pilnuj graczy przed zlamaniem regulaminu ]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "admin",
   max = 9999,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
customCheck = function(ply) return CLIENT or
   table.HasValue({"admin"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla Administratorow!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Administracja",
})
-- Mafia
TEAM_MAFIA = DarkRP.createJob("Czlonek Mafii", {
   color = Color(0, 94, 237, 255),
   model = {"models/player/Suits/male_09_closed_coat_tie.mdl"},
   description = [[Napadaj, zabijaj, rabuj!]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "soldat",
   max = 6,
   salary = 40,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
   
})
TEAM_CAPO = DarkRP.createJob("Szef Mafii", {
   color = Color(164, 0, 242, 255),
   model = {"models/player/Suits/male_01_closed_coat_tie.mdl"},
   description = [[Napadaj, zabijaj, rabuj!]],
   weapons = {"lockpick", "weapon_arc_atmcard", "selfie_taker"},
   command = "capo",
   max = 1,
   salary = 30,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
   
})
-- Zlodzieje
TEAM_ZLODZIEJ1 = DarkRP.createJob("Zlodziej", {
   color = Color(0, 0, 0, 255),
   model = {"models/player/Suits/robber_open.mdl"},
   description = [[Jestes Zlodziejem kradniesz , zabijasz , raidujesz i walmujesz sie mozesz tez zostac wynajety przez gang platnie do raidow.]],
   weapons = {"lockpick", "weapon_arc_atmcard", "selfie_taker", "pickpocket"},
   command = "zlodziej",
   max = 10,
   salary = 20,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
   
})

TEAM_ZLODZIEJ = DarkRP.createJob("Wlamywacz", {
   color = Color(0, 0, 0, 255),
   model = {"models/player/Suits/male_04_closed_coat_tie.mdl"},
   description = [[Jestes Wlamywaczem nie mozesz krasc , zabijac , raidowac i walmywac ,ale mozesz zostac wynajety przez gang platnie do raidow.]],
   weapons = {"lockpick", "weapon_arc_atmcard", "keypad_cracker", "selfie_taker"},
   command = "wlamywacz",
   max = 10,
   salary = 40,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
   
})

-- Policja
TEAM_SZEFP = DarkRP.createJob("Szef Policji", {
   color = Color(5, 0, 255, 255),
   model = {"models/player/police.mdl"},
   description = [[Jako szef policji rozkazujesz policjantom co maja robic , patrolowac czy moze przeszukac dom podejrzanego za twoja zgoda.]],
   weapons = {"fas2_mp5k", "door_ram", "darkrp_handcuffs", "realbrn_tazer_mr",  "weaponchecker",  "unarrest_stick", "stunstick",  "arrest_stick", "weapon_arc_atmcard", "selfie_taker"},
   command = "szefp",
   max = 1,
   salary = 50,
   admin = 0,
   vote = true,
   hasLicense = true,
   needchangefrom = (TEAM_PSY);
   candemote = true,
   medic = false,
   chief = true,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
-- Podpalacz
TEAM_FLTH = DarkRP.createJob("Podpalacz", {
   color = Color(5, 0, 255, 255),
   model = {"models/cakez/wolfenstein/spacetrooper_p.mdl"},
   description = [[Podpal ludzi oraz propy.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "podpalacz",
   max = 3,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = true,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})
TEAM_PSY = DarkRP.createJob("Policjant", {
   color = Color(5, 0, 255, 255),
   model = {
   "models/player/uk_police/uk_police_01.mdl",
   "models/player/uk_police/uk_police_02.mdl",
   "models/player/uk_police/uk_police_03.mdl",
   "models/player/uk_police/uk_police_04.mdl",
   "models/player/uk_police/uk_police_05.mdl",
   "models/player/uk_police/uk_police_06.mdl",
   "models/player/uk_police/uk_police_07.mdl",
   "models/player/uk_police/uk_police_08.mdl",
   "models/player/uk_police/uk_police_09.mdl"
   },
   description = [[Jestes strozem prawa. Patrolujesz ulice w celu znalezienia przestepstw , gdy juz na takie trafisz musisz aresztowac lub upomniec podejrzanego.]],
   weapons = {"fas2_p226", "door_ram", "darkrp_handcuffs", "realrbn_tazer_mr",  "weaponchecker",  "unarrest_stick", "stunstick",  "arrest_stick", "weapon_arc_atmcard", "selfie_taker"},
   command = "policja",
   max = 6,
   salary = 40,
   admin = 0,
   vote = true,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
   
})

TEAM_GUNS = DarkRP.createJob("Sprzedawca Ciezkiej broni", {
   color = Color(255, 168, 0, 255),
   model = {"models/player/leet.mdl"},
   description = [[Sprzedawca Broni Ciezkiej sprzedaje graczom bron ciezka typu ak-47 , itp.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "sprzedawca",
   max = 2,
   salary = 40,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
   
})
TEAM_MOB = DarkRP.createJob("Hitman", {
   color = Color(175, 9, 9, 255),
   model = {"models/player/agent_47.mdl"},
   description = [[Hitman wykonuje zlecenia zabojstwa dla graczy.]],
   weapons = {"fas2_machete","weapon_arc_atmcard", "selfie_taker"},
   command = "hitman",
   max = 1,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,  
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})

TEAM_CITIZEN = DarkRP.createJob("Mieszkaniec", {
   color = Color(25, 140, 30, 255),
   model = {"models/player/Group01/male_02.mdl"},
   description = [[Jako Mieszkaniec jestes zwyklym cywilem , nie mozesz posiadac broni i nie mozesz raidowac,zabijac,krasc, itp.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "mieszkaniec",
   max = 100,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = false,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})

TEAM_JCAGE = DarkRP.createJob("Wlasciciel Kasyna", {
   color = Color(25, 140, 30, 255),
   model = {"models/johnnycage/mk_cage.mdl"},
   description = [[Jako wlascicel kasyna ustalasz podatki za uzycie maszyny.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "jcage",
   max = 2,
   salary = 60,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = false,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})

TEAM_MEDYK = DarkRP.createJob("Medyk", {
   color = Color(0, 255, 239, 255),
   model = {"models/redninja/pmedic01.mdl"},
   description = [[Jestes Medykiem leczysz ludzi za pieniadze albo zostajesz wynajety przez gang platnie.]],
   weapons = {"fas2_ifak","weapon_arc_atmcard", "selfie_taker", "darkrp_defibrillator"},
   command = "medyk",
   max = 3,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_MODERATOR = DarkRP.createJob("Moderator Na Sluzbie", {
   color = Color(255, 0, 7, 255),
   model = {"models/player/swat.mdl"},
   description = [[Pilnuj graczy przed zlamaniem regulaminu ]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "mod",
   max = 9999,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
customCheck = function(ply) return CLIENT or
   table.HasValue({"moderator"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla Moderator�w!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Administracja",
})
TEAM_BANK = DarkRP.createJob("Bankier", {
   color = Color(255, 244, 0, 255),
   model = {"models/player/kleiner.mdl"},
   description = [[Przetrzymaj drukarki oraz pieniadze w banku.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "bankier",
   max = 2,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_BANKMANAGER = DarkRP.createJob("Menadzer Banku", {
   color = Color(255, 244, 0, 255),
   model = {"models/player/group03/male_03.mdl"},
   description = [[Zarzadzaj bankiem oraz personelem banku nie zapomnij zatrudnic ochroniarza.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "mgrbank",
   max = 1,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_SADMIN = DarkRP.createJob("SuperAdmin Na Sluzbie", {
   color = Color(255, 0, 7, 255),
   model = {"models/player/hostage/hostage_02.mdl"},
   description = [[Rzadz i dziel :P.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "pay_day_baton"},
   command = "sadmin",
   max = 9999,
   salary = 0,
   admin = 2,
   vote = false,
   hasLicense = true,
   candemote = false,
customCheck = function(ply) return CLIENT or
   table.HasValue({"superadmin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla Super adminow!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Administracja",
})
TEAM_BUREK = DarkRP.createJob("Major", {
   color = Color(209, 47, 47, 255),
   model = {"models/player/breen.mdl"},
   description = [[[Jestes Majorem to ty ustalasz prawo w tym miescie u�ywajac komend "./placelaws" , "./addlaw tekst", "./removelaw numer" i rzadzisz cala policja oraz szefem policji.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "pay_day_baton"},
   command = "burek",
   max = 1,
   salary = 50,
   admin = 0,
   vote = true,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = true,
   mayor = true,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_GUNSL = DarkRP.createJob("Sprzedawca Lekkiej broni", {
   color = Color(255, 168, 0, 255),
   model = {"models/player/monk.mdl"},
   description = [[Sprzedawca Broni lekkiej sprzedaje graczom bronie lekkie takie jak granaty.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "lekkisprzedawca",
   max = 2,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
   
})
TEAM_HOBO = DarkRP.createJob("Bezdomny", {
   color = Color(90, 49, 6, 255),
   model = {"models/player/odessa.mdl"},
   description = [[Napierdalaj innych butami i zebraj o hajsy!]],
   weapons = {"weapon_angryhoboswep_boot", "weapon_bugbait", "weapon_arc_atmcard", "selfie_taker"},
   command = "bezdomny",
   max = 4,
   salary = 40,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = true,
   cook = false,
   category = "Cywile",
})

TEAM_SZKOLA = DarkRP.createJob("Nauczyciel wiremoda [VIP]", {
   color = Color(132, 214, 67, 255),
   model = {"models/player/hostage/hostage_01.mdl"},
   description = [[Nauczyciel Wiremoda slucha sie Dyrektora Wiremoda i uczy uczniow Wiremoda.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "nauczycielwiremoda",
   max = 10,
   salary = 500,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})

TEAM_SZKOLADYRO = DarkRP.createJob("Dyrektor szkoly wiremoda [VIP]", {
   color = Color(132, 214, 67, 255),
   model = {"models/player/magnusson.mdl"},
   description = [[Dyrektor Wiremoda kupuje szkole , naucza uczniow razem z nauczycielami ktorych zatrudnia.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "dyrowiremoda",
   max = 2,
   salary = 100,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_RAPE = DarkRP.createJob("Gwalciciel [VIP]", {
   color = Color(147, 147, 147, 255),
   model = {"models/player/charple.mdl"},
   description = [[Zgwalc caly serwer.]],
   weapons = {"weapon_arc_atmcard", "weapon_rape", "selfie_taker"},
   command = "rapeswep",
   max = 1,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})

TEAM_SZARY = DarkRP.createJob("Dealer Czarnego Rynku [VIP]", {
   color = Color(147, 147, 147, 255),
   model = {"models/player/group02/male_04.mdl"},
   description = [[Jako Dealer Czarnego Rynku mozesz sprzedawac nie legalnie graczom bronie typu lockpick , keypad chacker , itp.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "szary",
   max = 1,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})

TEAM_BARMAN = DarkRP.createJob("Barman", {
   color = Color(0, 255, 86, 255),
   model = {"models/player/group01/male_03.mdl"},
   description = [[Jako barman masz zapewnic naszym mieszkanca upicia sie oraz wiele innych rzeczy!]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "barman",
   max = 1,
   salary = 45,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_MECHANIK = DarkRP.createJob("Mechanik", {
   color = Color(112, 0, 255, 255),
   model = {"models/player/group01/male_03.mdl"},
   description = [[Naprawiaj auta!]],
   weapons = {"vc_repair" , "weapon_arc_atmcard", "selfie_taker"},
   command = "mechanik",
   max = 1,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_DJ = DarkRP.createJob("DJ (VIP)", {
   color = Color(132, 214, 67, 255),
   model = {"models/nate159/req/SWTFU/Dark_Stalker.mdl"},
   description = [[[DJ robi impreze w swoim klubie.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "dj",
   max = 1,
   salary = 60,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
	CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_OPRADIO = DarkRP.createJob("Operator Radia", {
   color = Color(132, 214, 67, 255),
   model = {"models/player/group01/male_06.mdl"},
   description = [[Operator Radia obsluguje radio ,by nadawac audycje dla graczy.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "opradio",
   max = 1,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_CINEMAOWNER = DarkRP.createJob("Wlasciciel Kina", {
   color = Color(132, 214, 67, 255),
   model = {"models/player/group01/male_01.mdl"},
   description = [[Wlasciciel Kina pokazuje smieszne , straszne lub zaskakujace filmy w kinie.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "ownercinema",
   max = 1,
   salary = 60,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_CINEMASALLER = DarkRP.createJob("Sprzedawca Kina", {
   color = Color(132, 214, 67, 255),
   model = {"models/player/group02/male_08.mdl"},
   description = [[Sprzedajesz popcorn oraz bilety!]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "cinemasaller",
   max = 3,
   salary = 60,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_PIANO = DarkRP.createJob("Pianista", {
   color = Color(132, 214, 67, 255),
   model = {"models/player/group03/male_07.mdl"},
   description = [[Pianista gra w publicznych wystepach na pianinie.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "pianista",
   max = 2,
   salary = 16,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_PERKUSIA = DarkRP.createJob("Perkusista", {
   color = Color(132, 214, 67, 255),
   model = {"models/player/group03/male_06.mdl"},
   description = [[Perkusista gra w publicznych wystepach na perkusji.]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "perkusista",
   max = 2,
   salary = 16,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_METHA = DarkRP.createJob("Kucharz Mety", {
   color = Color(6, 42, 232, 255),
   model = {
   "models/dxn/cod_ghosts/hazmat_pm.mdl",
   "models/player/gasmaskcitizen/gasmask_civi_01playermodel.mdl"
   },
   description = [[Ziomek Gotuj mete :)]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "methacok",
   max = 3,
   salary = 40,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})
TEAM_METHVIP = DarkRP.createJob("Kucharz Mety+ (VIP)", {
   color = Color(6, 42, 232, 255),
   model = {"models/hazmat/bmhaztechs.mdl"},
   description = [[Kucharz mety specjalny z lepszym addonem :)]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "sent_tablet"},
   command = "methvip",
   max = 6,
   salary = 40,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})

TEAM_HACKER = DarkRP.createJob("Hacker (VIP)", {
   color = Color(168, 249, 240, 255),
   model = {"models/watch_dogs/characters/aiden_pearce.mdl"},
   description = [[Hacker [VIP] hackuje ATM ,by uzyska� z nich pieniadze.]],
   weapons = {"weapon_arc_atmcard", "weapon_hack_phone", "pcmod_pwcrack", "keypad_cracker", "selfie_taker"},
   command = "vip_hacker",
   max = 3,
   salary = 30,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})
TEAM_DRUGS = DarkRP.createJob("Handlarz Narkotykami", {
   color = Color(175, 220, 226, 255),
   model = {"models/metasync/sjainindain/sjain.mdl"},
   description = [[Sprzedawaj narkotyki :)]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "drugdeller",
   max = 3,
   salary = 30,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})
TEAM_FIREMAN = DarkRP.createJob("Strazak", {
   color = Color(214, 67, 67, 255),
   model = {"models/fearless/fireman2.mdl"},
   description = [[Strazak gasi pozary ktore znajduja sie najczesciej w budynkach.]],
   weapons = {"weapon_arc_atmcard", "weapon_extinguisher", "selfie_taker"},
   command = "fireman",
   max = 3,
   salary = 20,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_GAMERBOSS = DarkRP.createJob("Wlasciciel Salonu Gier", {
   color = Color(214, 67, 67, 255),
   model = {"models/gmod/male02_master.mdl"},
   description = [[Kupujesz budynek oraz automaty]],
   weapons = {"weapon_arc_atmcard", "weapon_extinguisher", "selfie_taker"},
   command = "gamerboss",
   max = 1,
   salary = 20,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_GAMERSELLER = DarkRP.createJob("Sprzedawca Salonu Gier", {
   color = Color(214, 67, 67, 255),
   model = {"models/player/xianghua/xianghua.mdl"},
   description = [[Sprzedajesz Itemy]],
   weapons = {"weapon_arc_atmcard", "weapon_extinguisher", "selfie_taker"},
   command = "gamerseller",
   max = 3,
   salary = 20,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
-- Jak dobrze pamietam tu potrzebny jest PCMOD 
TEAM_IT = DarkRP.createJob("Informatyk", {
   color = Color(214, 67, 67, 255),
   model = {"models/player/arnold_schwarzenegger.mdl"},
   description = [[Podlanczasz komputery oraz naprawiasz je za pieniadze :).]],
   weapons = {"weapon_arc_atmcard", "pcmod_installdisk", "pcmod_hdcopier", "selfie_taker"},
   command = "it",
   max = 3,
   salary = 40,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_TERROR = DarkRP.createJob("Terrorysta (VIP)", {
   color = Color(175, 220, 226, 255),
   model = {"models/player/hobo387/johnnygat.mdl"},
   description = [[terrorysta]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "usm_c4"},
   command = "terrorysta",
   max = 3,
   salary = 26,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = true,
customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})

TEAM_OCHRONA = DarkRP.createJob("Ochroniarz", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/zsecurity/zsecurity.mdl"},
   description = [[Jestes zatrudniony przez wlasciciela budynku oraz ochraniasz budynek]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "ochrona",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_SEDZIA = DarkRP.createJob("Sedzia", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/hostage/hostage_01.mdl"},
   description = [[Jestes sedzia i rozpatrzasz sprawy]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "sedzia_law",
   max = 3,
   salary = 140,
   admin = 0,
   vote = true,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_PRAWNIK = DarkRP.createJob("Prawnik", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/hostage/hostage_04.mdl"},
   description = [[Bronisz osobe]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "prawnik_law",
   max = 3,
   salary = 140,
   admin = 0,
   vote = true,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_PRO = DarkRP.createJob("Prokurator", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/hostage/hostage_03.mdl"},
   description = [[Kontrolujesz posiedzenia sadu]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "prokurator_law",
   max = 3,
   salary = 140,
   admin = 0,
   vote = true,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_SWAT = DarkRP.createJob("S.W.A.T (VIP)", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/riot.mdl"},
   description = [[Jako S.W.A.T ochraniasz budynki panstwowe]],
   weapons = {"weapon_arc_atmcard", "fas2_m4a1", "fas2_p226", "selfie_taker", "fas2_sr25"},
   command = "SWAT",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})

TEAM_OCHWIEZIENIA = DarkRP.createJob("Straznik Wiezienia", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/WTNO_FireTropper.mdl"},
   description = [[Jako Straznik Wiezienia zajmujesz sie pilnowaniem panstwowego wiezienia]],
   weapons = {"weapon_arc_atmcard", "fas2_m4a1", "fas2_p226", "selfie_taker", "fas2_sr25"},
   command = "ochwiezienia",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_KOMORNIK = DarkRP.createJob("Komornik", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/group01/male_04.mdl"},
   description = [[Zbieraj z podatkow pieniadze dla panstwa]],
   weapons = {"weapon_arc_atmcard", "fas2_m4a1", "fas2_p226", "selfie_taker", "fas2_sr25", "stunstick"},
   command = "komornik",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_SEKRETARKA = DarkRP.createJob("Sekretarka Majora", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/p2_chell.mdl"},
   description = [[Jestes sekretarka majora]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "sekretarka",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_SAPER = DarkRP.createJob("SAPER", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/cell03.mdl"},
   description = [[Jak najszybciej rozbrajasz bomby]],
   weapons = {"weapon_arc_atmcard", "fas2_m4a1", "fas2_p226", "selfie_taker", "fas2_sr25", "stunstick"},
   command = "saper",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   needchangefrom = (TEAM_PSY);
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_ZABOJCZYNI = DarkRP.createJob("Zabojczyni [VIP]", {
   color = Color(204, 164, 164, 255),
   model = {"models/xmp/player/artemis_pilot.mdl"},
   description = [[Jako zabojczyni zabijasz platnie osoby]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "fas2_m67", "weapon_crossbow"},
   command = "zabojczyni",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})
TEAM_DWIEZIENIA = DarkRP.createJob("Dozorca Wiezienia [VIP]", {
   color = Color(204, 164, 164, 255),
   model = {"models/player/cell04.mdl"},
   description = [[Jako dozorca wiezienia kontrolujesz swoje wiezienie oraz zatrudniasz Straznika wiezienia]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "fas2_m67", "weapon_crossbow"},
   command = "NWiezienia",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_SNAJPER = DarkRP.createJob("Snajper [VIP]", {
   color = Color(204, 164, 164, 255),   
   model = {"models/player/cell02.mdl"},
   description = [[Jako snajper udzielasz sie w dzialania policji]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "fas2_sr25"},
   command = "Snajper",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_GORNIK = DarkRP.createJob("Gornik", {
   color = Color(204, 164, 164, 255),   
   model = {"models/player/scavenger/scavenger.mdl"},
   description = [[Jako Gornik kopiesz rude]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "eoti_tool_miningpick"},
   command = "gornik",
   max = 3,
   salary = 10,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cywile",
})
TEAM_MHOTEL = DarkRP.createJob("Menadzer Hotelu [VIP]", {
   color = Color(204, 164, 164, 255),   
   model = {"models/player/suits/male_06_shirt.mdl"},
   description = [[Kupujesz budynek oraz sprzedajesz pokoje]],
   weapons = {"weapon_arc_atmcard", "selfie_taker"},
   command = "Hotel",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Panstwo",
})
TEAM_PSYCHO = DarkRP.createJob("Psychopata [VIP]", {
   color = Color(204, 164, 164, 255),   
   model = {"models/player/r6s_jager.mdl"},
   description = [[Zabijasz jak popada]],
   weapons = {"weapon_arc_atmcard", "selfie_taker", "rchainsaw"},
   command = "Psychopata",
   max = 3,
   salary = 140,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = true,
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin","owner"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Gangsterka",
})
--- TAK Wiem customcheck w komentarzy specjalnie zostawilem :P
--[[----------------------------
   customCheck = function(ply) return CLIENT or
   table.HasValue({"vip","superadmin","moderator","admin"}, ply:GetNWString("usergroup"))
end,
CustomCheckFailMsg = "Ta praca jest tylko dla VIP!",
--]]-----------------------------

-- Domyslna druzyna przy spawn
GAMEMODE.DefaultTeam = TEAM_CITIZEN


-- Druzyna z mozliwoscia /warrant /wanted 
GAMEMODE.CivilProtection = {
	[TEAM_PSY] = true,
	[TEAM_SZEFP] = true,
	[TEAM_BUREK] = true,
	[TEAM_FIREMAN] = true,
	[TEAM_OCHWIEZIENIA] = true,
	[TEAM_DWIEZIENIA] = true,
	[TEAM_SEDZIA] = true,
	[TEAM_PRAWNIK] = true,
	[TEAM_PRO] = true,
	[TEAM_SNAJPER] = true,
	[TEAM_SWAT] = true,
	[TEAM_KOMORNIK] = true,
	[TEAM_SEKRETARKA] = true,
	[TEAM_SAPER] = true,
}

--Prace z dostepem do menu hitmana
DarkRP.addHitmanTeam(TEAM_MOB)
DarkRP.addHitmanTeam(TEAM_ZABOJCZYNI)
