
include('shared.lua')
local iButtons = {}
function addBtn(text,info)
	iButtons[text] = {text = text,info = info}
end


--[[

 To add a button. Type "addBtn('TEXT',"INFORMATION HERE')" You can make as much as you want. The buttons will keep stacking over and over. 

 Coded by: D3Networking 

]]

addBtn('Witamy na serwerze GamesCitadel!','Witaj uzytkowniku mamy wiele rzeczy do pokazania ale najpierw przeczytaj regulamin na stronie https://gamescitadel.eu/forum/showthread.php?tid=124')
addBtn('Jak zagrac na darkRP?','DarkRP to przedewszystkim symulacja realnego zycia wszystkie prace mozesz znalezc pod F4 oraz popytaj twoich znajomych na serwerze :).')
addBtn('Opiekunowie Serwera?','Opiekunami serwera sa MateuszNET & Fizi!')
addBtn('Co daje VIP?','VIP daje dostep do prac VIP oraz nie ktorych dodatkow VIP takie jak zawansowane gotowanie mety i pomaga rozwinac siec serwerow !')
addBtn('Jak kupic vipa?','Chcia�by� VIP zg�o� si� do MateuszNET , Fizi oraz Zaire Adams  (Pracujemy nad auto vipem )!')
addBtn('Forum Serwera?','gamescitadel.eu/forum')
addBtn('Grupa STEAM Serwera?','http://steamcommunity.com/groups/gamescitadel')
addBtn('TS3 Serwera?','ip serwera ts3| gamescitadel.eu')
addBtn('Koszt VIP?','7zl/msc stala kwota ')
addBtn('Tworcy Serwera (Credits)','Prace oraz Ustawiena darkrp : MateuszNET ')
addBtn('Ktos mi przeszkadza podczas budowy !','Uzyj komendy /build ktora wylaczy cie z rp oraz pokazuje napis nie przeszkadzac')



------------------------------------------------------------------------------------------------------------------------
 
--[[ The text printed into chat ]] ChatTextMessage = "Witaj! , Co chcialbys wiedziec?"   
--[[ The COlor of the chat text ]] ChatTextMessageColor = Color(255, 255, 255)

--[[ The text printed into chat ]] DoneChatTextMessage = "Dowidzenia, Czas na mnie!" 
--[[ The COlor of the chat text ]] DoneChatTextMessageColor = Color(255, 255, 255)
 
--[[ The Title ]] Title = "Przewodnik NPC" -- The Title 

--[[What text displays over the buttons?]] TextChat = "Witaj! , Co chcialbys wiedziec?"

local RandomSounds = {
	
"vo/npc/female01/answer01.wav",
"vo/npc/female01/answer02.wav",
"vo/npc/female01/answer03.wav",
"vo/npc/female01/answer04.wav",
"vo/npc/female01/answer05.wav",
"vo/npc/female01/answer07.wav",
"vo/npc/female01/answer08.wav",
"vo/npc/female01/answer09.wav",
"vo/npc/female01/answer10.wav",
	
	
} -- Want to add your own sounds to play when you use the NPC Guide? Just add the sound file path here! 

------------------------------------------------------------------------------------------------------------------------

function ENT:Initialize()
	self.Color = Color( 255, 255, 255, 255 ) 
end

local function DrawTextAH(ent)
	local zOffset = 50
	local x = ent:GetPos().x			//Get the X position of our player
	local y = ent:GetPos().y			//Get the Y position of our player
	local z = ent:GetPos().z			//Get the Z position of our player
	local pos = Vector(x,y,z+zOffset)	//Add our offset onto the Vector
	local pos2d = pos:ToScreen()		//Change the 3D vector to a 2D one
	draw.DrawText("NPC GUIDE","HUDNumber5",pos2d.x,pos2d.y,Color(255,255,255,255),TEXT_ALIGN_CENTER)	//Draw the indicator
end

hook.Add("HUDPaint", "LoopThroughPlayers", function()	//Add our function to the HUDPaint hook
	for k, v in pairs( ents.FindByClass( "npcguide" ) ) do
		if(LocalPlayer():GetPos():Distance(v:GetPos()) < 220) then
			DrawTextAH(v)
		end
	end
end)

function CreateThickOutlinedBox( x, y, w, h, thickness, clr )
	surface.SetDrawColor( clr )
	for i=0, thickness - 1 do
		surface.DrawOutlinedRect( x + i,  y + i, w - i * 2, h - i * 2 )
	end
end

------------------------------------------------------------------------------------------------------------------------

local function NPCShopUI()

	surface.PlaySound(table.Random(RandomSounds))

	chat.AddText(Color(255,186,5), "Citzen: ",ChatTextMessageColor, ChatTextMessage )
	
	local NPCUI = vgui.Create( "DFrame" )
		NPCUI:SetPos( ScrW()*0.35, ScrH()*0.75 )
		NPCUI:SetSize( 545, 200 )
		NPCUI:SetTitle( "" )
		NPCUI:SetAlpha(0)
		NPCUI:SetBackgroundBlur( true )
		NPCUI:ShowCloseButton(false)
		NPCUI:SetDraggable( false )
		NPCUI:MakePopup()
		NPCUI.Paint = function(self,w,h)
			draw.RoundedBox(0,0,0,w,h,Color(0,0,0,245))
			CreateThickOutlinedBox( 0,0,w,h,1,Color(0,150,0) )
		end
		
		NPCUI:AlphaTo(255,0.3,0)
		
			local SelectedBox = vgui.Create( "DFrame",NPCUI )
		SelectedBox:SetPos( ScrW()*0.35, ScrH()*0.625 )
		SelectedBox:SetSize( 545, 35 )
		SelectedBox:SetTitle( "" )
		SelectedBox:ShowCloseButton(false)
		SelectedBox:SetDraggable( false )
		SelectedBox:MakePopup()
		SelectedBox.Paint = function(self,w,h)
			draw.RoundedBox(0,0,0,w,h,Color(0,0,0,245))
			CreateThickOutlinedBox( 0,0,w,h,1,Color(0,150,0) )
		end

		local richtext = vgui.Create( "RichText", SelectedBox )
		richtext:Dock( FILL )
		richtext:InsertColorChange( 192, 192, 192, 255 )
		richtext:AppendText( "Welcome to the server!" )

		function richtext:PerformLayout()
			self:SetFontInternal( "ChatFont" )
			self:SetFGColor( Color( 255, 255, 255 ) )
		end

		local DermaList = vgui.Create( "DPanelList", NPCUI )
		DermaList:SetPos( 5,30 )
		DermaList:SetSize( NPCUI:GetWide()-10, NPCUI:GetTall()-35 )
		DermaList:SetSpacing( 5 ) -- Spacing between items
		DermaList:EnableHorizontal( false ) -- Only vertical items
		DermaList:EnableVerticalScrollbar( true ) -- Allow scrollbar if you exceed the Y axis

		local isSwitching = false

		for k,v in pairs(iButtons) do
			local DButton = vgui.Create( "DButton",layout )
			DButton:SetText( v.text )
			DButton:SetSize( 300, 25 )
			DButton:SetTextColor(color_white)
			DButton.DoClick = function()
				print( "Przycisles to juz!" )
			end
			DButton.Paint = function(self,w,h)
				draw.RoundedBox(0,0,0,w,h,Color(100,100,100,245))
				CreateThickOutlinedBox( 0,0,w,h,1,Color(150,150,150) )
			end
			
			DButton.DoClick = function()
				
				if isSwitching then return end
				isSwitching = true
				SelectedBox:SizeTo( 545, 35, 0.3, 0)
				richtext:SetText('')
				timer.Simple(0.65,function()
					richtext:SetText(v.info)
					SelectedBox:SizeTo( 545, 100, 0.3, 0 )
					timer.Simple(0.3,function() isSwitching = false end)
				end)
			end
			DermaList:AddItem(DButton)
		end
	

			local CDButton = vgui.Create( "DButton",layout )
			CDButton:SetText( "Zamknij Okno!" )
			CDButton:SetSize( 300, 25 )
			CDButton:SetTextColor(color_white)
			CDButton.DoClick = function()
				if isSwitching then return end
				surface.PlaySound(table.Random(RandomSounds))
				NPCUI:Close()
			end
			CDButton.Paint = function(self,w,h)
				draw.RoundedBox(0,0,0,w,h,Color(100,100,100,245))
				CreateThickOutlinedBox( 0,0,w,h,1,Color(150,150,150) )
			end
			DermaList:AddItem(CDButton)
end
usermessage.Hook("DrawNPCGUI",NPCShopUI)

function ENT:Draw()
	
	--self:DrawEntityOutline( 1 )
	self.Entity:DrawModel()

end