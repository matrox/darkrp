local model = "models/alyx.mdl" -- What model should it be?
local classname = "npcguide" -- This should be the name of the folder containing this file.
local ShouldSetOwner = false -- Set the entity's owner?

local model = { }

-------------------------------
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )
-------------------------------

--------------------
-- Spawn Function --
--------------------
function ENT:SpawnFunction( ply, tr )

	if ( !tr.Hit ) then return end
	local SpawnPos = tr.HitPos + tr.HitNormal * 25
	local ent = ents.Create( classname )
	ent:SetPos( SpawnPos )
	ent:Spawn()
	ent:Activate()
	if ShouldSetOwner then
		ent.Owner = ply
	end
	return ent
	
end

----------------
-- Initialize --
----------------

local NPCGuideModels = {

"models/Humans/Group01/Female_01.mdl",
"models/Humans/Group01/Female_02.mdl",
"models/Humans/Group01/Female_03.mdl",
"models/Humans/Group01/Female_04.mdl",
"models/Humans/Group01/Female_06.mdl",
"models/Humans/Group01/Female_07.mdl"


}

function ENT:Initialize( )
 
	self:SetModel( table.Random(NPCGuideModels) )
	self:SetHullType( HULL_HUMAN )
	self:SetHullSizeNormal( )
	self:SetNPCState( NPC_STATE_SCRIPT )
	self:SetSolid(  SOLID_BBOX )
	self:CapabilitiesAdd(CAP_ANIMATEDFACE)
	self:SetUseType( SIMPLE_USE )
	self:DropToFloor()

	self:SetMaxYawSpeed( 90 )
 
end

function ENT:AcceptInput( Name, Activator, Caller )
	if Name == "Use" and Caller:IsPlayer() then
		umsg.Start("DrawNPCGUI", Caller) 
		umsg.End()
	end

end
-----------------
-- Take Damage -- 
-----------------
function ENT:OnTakeDamage( dmginfo )
	self.Entity:TakePhysicsDamage( dmginfo )

end

------------
-- On use --
------------
function ENT:Use( activator, caller )

end

-----------
-- Think --
-----------
function ENT:Think()

end

-----------
-- Touch --
-----------
function ENT:Touch(ent)

end

--------------------
-- PhysicsCollide -- 
--------------------
function ENT:PhysicsCollide( data, physobj )

end