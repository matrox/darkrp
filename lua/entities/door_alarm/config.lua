--[[
    __    ____  _____ _______ _____ ____  _   _    ____  _   _ _  __     _____  _ 
   /_ |  / __ \|  __ \__   __|_   _/ __ \| \ | |  / __ \| \ | | | \ \   / /__ \| |
    | | | |  | | |__) | | |    | || |  | |  \| | | |  | |  \| | |  \ \_/ /   ) | |
    | | | |  | |  ___/  | |    | || |  | | . ` | | |  | | . ` | |   \   /   / /| |
    | | | |__| | |      | |   _| || |__| | |\  | | |__| | |\  | |____| |   |_| |_|
    |_|  \____/|_|      |_|  |_____\____/|_| \_|  \____/|_| \_|______|_|   (_) (_)
                                                                                
    YES! There's not much to edit and to be honest, don't expect a lot more from
    upcoming updates. Much of the things inside this addon I DO NOT want you to edit,
    that's why the config is so short.

    By the way, if you are wondering why there's no option to change the sound, It's very simple.
    Many people would scream at me saying that their sound is not looping, so It's not here to evade problems!
    However if you know lua you can do the process by yourself, go ahead line 102. I won't help with this, so don't ask!

    Got any bugs to report? Find n00bmobile, hunt him down! HE MUST FIX IT! 
	
]]--

AlarmConfig = {
    ["LockpickTimeVariation"] = {25, 25}, //The special lockpick time with variation when lockpicking a secured door.
	["TotalHealth"] = 500 //How much health an alarm has.
}