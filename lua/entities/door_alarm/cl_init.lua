include("shared.lua")

function ENT:Initialize()
	self.Color = Color( 255, 255, 255, 255 )
end

surface.CreateFont("AlarmFont", {font = "Default", size = 20})
function ENT:Draw() 
    self.Entity:DrawModel()
	
	local pos = self:GetPos()
	local ang = self:GetAngles()
	
	local pos2 = self:GetPos()
	local ang2 = self:GetAngles()
	
	local pos3 = self:GetPos()
	local ang3 = self:GetAngles()
	
	ang:RotateAroundAxis(ang:Right(), 90)
	
	ang2:RotateAroundAxis(ang2:Up(), 90)
	ang2:RotateAroundAxis(ang2:Forward(), 90)
	
    ang3:RotateAroundAxis(ang3:Right(), 90)
	ang3:RotateAroundAxis(ang3:Forward(), 180)
	
	--[[
	[HOLY MOTHER OF STRINGS]
	
	I understand using strings as bools isn't a good thing, they're not made for that.
	But here's the thing, if anyone touches any of these string the addon will fuck up like never before.
	If someone wishes to translate this, contact me and I'll start working on a translation system.
	]]

	//Black Background
	cam.Start3D2D(pos +ang:Up() *-5.8, ang, 0.30)
		draw.RoundedBox(0, -6, 0, 9, 17, Color(0, 0, 0))
	cam.End3D2D()

	//TV Background
	cam.Start3D2D(pos2 +ang:Up() *-5.8, ang2, 0.05)
		if (self:GetNWString("DAlarm_Status") != "Active" && self:GetNWString("DAlarm_Status") != "Inactive" && self:GetNWString("DAlarm_Status") != "WARNING" && self:GetNWString("DAlarm_Status") != "") then
			surface.SetDrawColor(0, 0, 0)
	            surface.SetMaterial(Material("effects/security_noise2"))
	        surface.DrawTexturedRect(-105, -17, 100, 50)
		end
	cam.End3D2D()
	
	//Hacking Window
	cam.Start3D2D(pos +ang:Up() *-5.8, ang, 0.30)
	    if (self:GetNWString("DAlarm_Status") != "Active" && self:GetNWString("DAlarm_Status") != "Inactive" && self:GetNWString("DAlarm_Status") != "WARNING" && self:GetNWString("DAlarm_Status") != "") then
		    draw.RoundedBox(0, -3, 2, 4, 14, Color(75, 75, 75))
		    draw.RoundedBox(0, 1, 2, 1, 14, Color(25, 25, 170))
		end
	cam.End3D2D()
	
	//Hacking Window Title
	cam.Start3D2D(pos2 +ang:Up() *-5.8, ang2, 0.016)
		if (self:GetNWString("DAlarm_Status") != "Active" && self:GetNWString("DAlarm_Status") != "Inactive" && self:GetNWString("DAlarm_Status") != "WARNING" && self:GetNWString("DAlarm_Status") != "") then
		    draw.SimpleText("Password Cracker", "AlarmFont", -170, -30, Color(255, 255, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	    end
	cam.End3D2D()
	
	//Texts
	cam.Start3D2D(pos2 +ang:Up() *-5.8, ang2, 0.05)
		if (self:GetNWString("DAlarm_Status") != "Active" && self:GetNWString("DAlarm_Status") != "Inactive" && self:GetNWString("DAlarm_Status") != "") then
		    if (self:GetNWString("DAlarm_Status") == "WARNING") then
			    draw.SimpleText(self:GetNWString("DAlarm_Status"), "AlarmFont", -54, 7, Color(255, 255, 0, 255 -math.sin(CurTime() *5) *255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			else
		        draw.SimpleText(string.Replace(self:GetNWString("DAlarm_Status"), "*", tostring(math.random(1, 9))), "AlarmFont", -54, 05, Color(255, 255, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		    end
		else
			draw.SimpleText(self:GetNWString("DAlarm_Status"), "AlarmFont", -54, 7, Color(255, 255, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		end
	cam.End3D2D()
	
	//Tearing Left
	cam.Start3D2D(pos +ang:Up() *-5.8, ang, 0.30)
		if (self:GetNWString("DAlarm_Status") != "Active" && self:GetNWString("DAlarm_Status") != "Inactive" && self:GetNWString("DAlarm_Status") != "WARNING" && self:GetNWString("DAlarm_Status") != "") then
		    for i = 1, string.find(self:GetNWString("DAlarm_Status"), "*", 1) do
	            draw.RoundedBox(0, math.random(-6, 2), 0, 1, math.random(1, 17), Color(0, 0, 0))
            end
        end			
	cam.End3D2D()
	
	//Tearing Right
	cam.Start3D2D(pos3 +ang:Up() *-5.8, ang3, 0.30)
		if (self:GetNWString("DAlarm_Status") != "Active" && self:GetNWString("DAlarm_Status") != "Inactive" && self:GetNWString("DAlarm_Status") != "WARNING" && self:GetNWString("DAlarm_Status") != "") then
		    for i = 1, string.find(self:GetNWString("DAlarm_Status"), "*", 1) do
			    draw.RoundedBox(0, math.random(-6, 2), -17, 1, math.random(1, 17), Color(0, 0, 0))
            end
        end		
	cam.End3D2D()
end