AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("config.lua")

include("shared.lua")

DoorAlarm = {}

function ENT:Initialize()
	if (self.Entity:Health() > 0) then
	    self.Entity:SetHealth(self.Entity:Health())
    else
        self.Entity:SetHealth(AlarmConfig["TotalHealth"])
    end
	
	self.Entity:SetNWString("DAlarm_Status", "Inactive")
	self.Entity:SetModel("models/props_lab/reciever01d.mdl")
	self.Entity:SetSolid(SOLID_VPHYSICS)
	self.Entity:SetMoveType(SOLID_VPHYSICS)
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetUseType(SIMPLE_USE)
	
	local Physics = self.Entity:GetPhysicsObject()
	    
	if (Physics:IsValid()) then
		Physics:Wake()
	end
end

function ENT:Touch(ent)
	if (!IsValid(ent.Secured)) then
	    if (ent:GetClass() == "prop_door_rotating") then
		    if (ent:isMasterOwner(self.AlarmOwner)) then 
		        local ang = ent:GetAngles()
		        local pos = ent:GetPos()
	
	            local dAlarm = ents.Create("door_alarm")
                
				dAlarm:EmitSound("npc/dog/dog_servo12.wav")
			    dAlarm:SetHealth(self:Health())
			    dAlarm:SetAngles(ang)
			    dAlarm:SetPos(pos +ang:Right() *-40 +ang:Up() *1 +ang:Forward() *-1.5)
		        dAlarm:SetParent(ent)
			    dAlarm:Spawn()
			    
				dAlarm.AlarmOwner = self.AlarmOwner
			    dAlarm.Securing = ent
			    dAlarm.DuringLockpick = false
			    dAlarm.Flipped = false
			
			    ent.Secured = dAlarm
			    self:Remove()
			
			    if (ent:isLocked()) then
				    dAlarm:SetNWString("DAlarm_Status", "Active")
			    else
			        dAlarm:SetNWString("DAlarm_Status", "Inactive")
			    end
			
			    DarkRP.notify(dAlarm.AlarmOwner, 3, 10, "This alarm is now coupled to this door, use /flipalarm to flip it around in case you need to!")
		    end
		end
	end
end

function ENT:OnTakeDamage(dmg)
	if (self:GetNWString("DAlarm_Status") == "Active") then
	    DoorAlarm.AlarmSound = CreateSound(self, "ambient/alarms/alarm1.wav")
		DoorAlarm.AlarmSound:Play()
	
	    self:SetNWString("DAlarm_Status", "WARNING")
	
	    timer.Create("JesusFuckLoopsTheySuckHard:"..self:EntIndex(), 1, 0, function() //Repeat kept crashing, so here's an amazing workaround!
			if (self:Health() > 0) then
			    if (self:GetPos():Distance(self.AlarmOwner:GetPos()) <= 100) then
			        DoorAlarm.AlarmSound:Stop()
	
			        if (self.Securing:getDoorData().title != "" && self.Securing:getDoorData().title != nil) then
			            DarkRP.notify(self.AlarmOwner, 0, 5, "Your alarm at "..self.Securing:getDoorData().title.." has returned to normal due to your proximity!")
			        else
				        DarkRP.notify(self.AlarmOwner, 0, 5, "Your alarm at an unnamed door has returned to normal due to your proximity!")
                    end
					
				    if (self.Securing:isLocked()) then
					    self:SetNWString("DAlarm_Status", "Active")
				    else
					    self:SetNWString("DAlarm_Status", "Inactive")
				    end
				
				    timer.Remove("JesusFuckLoopsTheySuckHard:"..self:EntIndex())
				end
			else
		        timer.Remove("JesusFuckLoopsTheySuckHard:"..self:EntIndex())
			end
		end)
		
		if (self.Securing:getDoorData().title != "" && self.Securing:getDoorData().title != nil) then
			DarkRP.notify(self.AlarmOwner, 0, 5, "Your alarm at "..self.Securing:getDoorData().title.." is taking damage!")
		else
			DarkRP.notify(self.AlarmOwner, 0, 5, "Your alarm at an unnamed door is taking damage!")
        end
	end
	
	if (dmg:GetAttacker():IsPlayer() && !dmg:GetAttacker():isWanted()) then
	    dmg:GetAttacker():wanted(nil, "Shooting Door Alarms!")
	end
	
	self:SetHealth(self:Health() -dmg:GetDamage())
		
	if (self:Health() <= 0) then
	    self:AlarmExplosion()
	end
end

function ENT:Use(ply)
    if (self.Securing) then
	    if (self.AlarmOwner == ply ) then
	        if (self:GetNWString("DAlarm_Status") != "Active" && self:GetNWString("DAlarm_Status") != "Inactive") then
		        DarkRP.notify(ply, 1, 5, "You cannot uncouple this alarm now!")
		    else
		        DarkRP.notify(ply, 0, 5, "You started uncoupling this alarm")
			    self:AlarmUncouple(ply)
		    end
	    else
		    DarkRP.notify(ply, 1, 5, "You do not own this alarm!")
		end
	end
end 

function ENT:OnRemove() //Preventing Lua Errors since 2015!
    //Destroy all timers to evade errors...
	timer.Remove("JesusFuckLoopsTheySuckHard:"..self:EntIndex())
	timer.Remove("Sparks: "..self:EntIndex())
	timer.Remove("AlarmHacking: "..self:EntIndex())
	timer.Remove("ReturnToNormal:"..self:EntIndex())
	
	//Stop all sounds coming from the entity that otherwise would loop forever.
	if (DoorAlarm.AlarmSound) then
	    DoorAlarm.AlarmSound:Stop()
	end
	
	if (DoorAlarm.DeniedSound) then
	    DoorAlarm.DeniedSound:Stop()
	end
	
	if (DoorAlarm.HackSound) then
	    DoorAlarm.HackSound:Stop()
	end
end

function ENT:AlarmUncouple(owner)
	--[[
	local ManSpark = EffectData()
			
	ManSpark:SetOrigin(self:GetPos())
	util.Effect("ManhackSparks", ManSpark)
	
	self:EmitSound("weapons/stunstick/alyx_stunner1.wav")
	self:SetNWString("DAlarm_Status", "")]]

	local SparkTimes = math.random(5, 10)
	
	timer.Create("Sparks: "..self:EntIndex(), 1, SparkTimes, function() //For some fucked up reason timer.Create() didn't accept to work alone, so I gave him a very special name so everyone else would bully him. Then he started to work alone.
		local Spark = EffectData()
			
		Spark:SetOrigin(self:GetPos())
		util.Effect("StunstickImpact", Spark)
		
		if (timer.RepsLeft("Sparks: "..self:EntIndex()) == 0) then
		    local dAlarm = ents.Create("door_alarm")
		
		    dAlarm.AlarmOwner = owner
		    
			dAlarm:SetHealth(self:Health())
			dAlarm:SetAngles(self:GetAngles())
		    dAlarm:SetPos(self:GetPos())
		    dAlarm:Spawn()
			
		    self:EmitSound("weapons/stunstick/alyx_stunner1.wav")
		    self:Remove()
		else
		    SparkSounds = {"weapons/stunstick/spark1.wav", "weapons/stunstick/spark2.wav", "weapons/stunstick/spark3.wav"}
			
		    local SparkSound = math.random(1, #SparkSounds)//He's not using table.Random()! KILL HIM, KILL HIM WITH FIRE! It wasn't working, so I did this workaround.
			
		    self:EmitSound(table.concat(SparkSounds, "", SparkSound, SparkSound))
		end
	end)
end

function ENT:AlarmExplosion()
	local Explosion = EffectData()
			
    Explosion:SetOrigin(self:GetPos())
	util.Effect("Explosion", Explosion)

	if (self.Securing) then
	    if (self.Securing:getDoorData().title != "" && self.Securing:getDoorData().title != nil) then
		    DarkRP.notify(self.AlarmOwner, 1, 5, "Your alarm at "..self.Securing:getDoorData().title.." has been destroyed!")
  	    else
		    DarkRP.notify(self.AlarmOwner, 1, 5, "Your alarm at an unamed door has been destroyed!")
	    end
	else
	    DarkRP.notify(self.AlarmOwner, 1, 5, "Your uncoupled alarm has been destroyed!")
	end
	
	self:Remove()
end

hook.Add("lockpickTime", "DAlarmChangeLockpickTime", function(ply, ent)
    if (ent.Secured && ent.Secured:GetNWString("DAlarm_Status") == "Active") then
        ent.Secured.DuringLockpick = true
		
		local LockpickTime = math.random(AlarmConfig["LockpickTimeVariation"][1], AlarmConfig["LockpickTimeVariation"][2])
		local Password = {"*", "*", "*", "*", "*", "*", "*", "*"}
		local PasswordCaracter = 0
		local HackPitch = 100
		
		DoorAlarm.DeniedSound = CreateSound(ent.Secured, "ambient/alarms/combine_bank_alarm_loop4.wav")
		DoorAlarm.AlarmSound = CreateSound(ent.Secured, "ambient/alarms/alarm1.wav")
		DoorAlarm.HackSound = CreateSound(ent.Secured, "ambient/energy/electric_loop.wav")
		
		DoorAlarm.AlarmSound:Play()
		DoorAlarm.HackSound:Play()
	
	    timer.Create("AlarmHacking: "..ent.Secured:EntIndex(), LockpickTime /#Password, 0, function()
			if (PasswordCaracter == #Password -1) then
				DoorAlarm.AlarmSound:Stop()
			    DoorAlarm.HackSound:Stop()
					
				timer.Remove("AlarmHacking: "..ent.Secured:EntIndex())
			elseif (ent.Secured.DuringLockpick) then
				PasswordCaracter = PasswordCaracter +1
				
				table.remove(Password, PasswordCaracter)
			    table.insert(Password, PasswordCaracter, math.random(1, 9))
				
				HackPitch = HackPitch +8
				DoorAlarm.HackSound:ChangePitch(HackPitch, 1)

				ent.Secured:SetNWString("DAlarm_Status", table.concat(Password, ""))
				ent.Secured:EmitSound("buttons/blip1.wav")
			else
			    if (PasswordCaracter == 0) then
					DoorAlarm.AlarmSound:Stop()
					DoorAlarm.HackSound:Stop()
					
					DoorAlarm.DeniedSound:Play()
					
					ent.Secured:SetNWString("DAlarm_Status", "WARNING")
					
					timer.Create("ReturnToNormal:"..ent.Secured:EntIndex(), 20, 1, function() //This could easily be a timer.Simple(), but I need full control over all my timers so I can't let that happen.
				        if (ent:isLocked()) then
						    ent.Secured:SetNWString("DAlarm_Status", "Active")
					    else
						    ent.Secured:SetNWString("DAlarm_Status", "Inactive")
					    end
                          
                        DoorAlarm.DeniedSound:Stop()						  
				    end)
					
					if (ent:getDoorData().title != "" && ent:getDoorData().title != nil) then
			            DarkRP.notify(ent.Secured.AlarmOwner, 0, 5, "Your alarm at "..ent:getDoorData().title.." has caught a hacker!")
				        DarkRP.notify(ply, 1, 5, "The alarm at "..ent:getDoorData().title.." has caught you hacking!")
					else
				        DarkRP.notify(ent.Secured.AlarmOwner, 0, 5, "Your alarm at an unnamed door has caught a hacker!")
						DarkRP.notify(ply, 1, 5, "The alarm at an unamed door has caught you hacking!")
                    end
					
				    ply:wanted(nil, "Trying to Hack Door Alarms!")
					
					timer.Remove("AlarmHacking: "..ent.Secured:EntIndex())
				else
				    table.remove(Password, PasswordCaracter)
			        table.insert(Password, PasswordCaracter, "*")
				
				    PasswordCaracter = PasswordCaracter -1
				
				    HackPitch = HackPitch -10
				    DoorAlarm.HackSound:ChangePitch(HackPitch, 1)

					ent.Secured:SetNWString("DAlarm_Status", table.concat(Password, ""))
				    ent.Secured:EmitSound("buttons/weapon_cant_buy.wav")
				end
			end
		end)
	
		if (ent:getDoorData().title != "" && ent:getDoorData().title != nil) then
		    DarkRP.notify(ent.Secured.AlarmOwner, 1, 5, "Your alarm at "..ent:getDoorData().title.." has been triggered!")
		else
		    DarkRP.notify(ent.Secured.AlarmOwner, 1, 5, "Your alarm at an unamed door has been triggered!")
		end
		
		ent.Secured:SetNWString("DAlarm_Status", table.concat(Password, ""))
		
		return LockpickTime
	end
end)

hook.Add("PlayerSwitchWeapon", "DAlarmFixDarkRPHookMalfunction", function(ply, wep) //DarkRP BUG! The Hook onLockpickCompleted doesn't get called when the player switches weapons.
    if (IsValid(wep)) then
	    if (wep:GetClass() == "lockpick") then
	        if (wep:GetLockpickEnt().Secured) then
		        hook.Call("onLockpickCompleted", nil, ply, false, wep:GetLockpickEnt())
		    end
		end
	end
end)

hook.Add("onLockpickCompleted", "DAlarmLockpickCompleted", function(ply, sucess, ent) //Buggy!! FPjte, please fix.
	if (ent.Secured) then
        if (ent.Secured.DuringLockpick) then
			ent.Secured.DuringLockpick = false
			
	     	if (sucess) then
                if (ent:getDoorData().title != "" && ent:getDoorData().title != nil) then
			        DarkRP.notify(ent.Secured.AlarmOwner, 1, 5, "Your alarm at "..ent:getDoorData().title.." has been hacked!")
				else
				    DarkRP.notify(ent.Secured.AlarmOwner, 1, 5, "Your alarm at an unnamed door has been hacked!")  
				end
				
				DarkRP.notify(ply, 0, 5, "You have reprogrammed this alarm and it is now yours!")
				ent.Secured:AlarmUncouple(ply)
		    else
			    timer.Adjust("AlarmHacking: "..ent.Secured:EntIndex(), 1, timer.RepsLeft("AlarmHacking: "..ent.Secured:EntIndex()))
			end
		end
	end
end)

hook.Add("canLockpick", "DAlarmBlockLockpickingDuringProtocol", function(ply, ent)
    if (ent:GetClass() == "prop_door_rotating") then
        if (ent.Secured) then
		    if (ent.Secured:GetNWString("DAlarm_Status") != "Active" && ent.Secured:GetNWString("DAlarm_Status") != "Inactive") then
			    DarkRP.notify(ply, 1, 5, "You cannot hack this alarm now!")
			    return false
			end
		end
	end
end)

hook.Add("onDoorRamUsed", "DAlarmBatRamUsed", function(sucess, ply, ent)
    if (sucess && ent.Entity.Secured) then
	    ent.Entity.Secured:AlarmExplosion()
	end
end)

hook.Add("GravGunPickupAllowed", "DAlarmDisableGravGunPickup", function(ply, ent)
    if (ent:GetClass() == "door_alarm") then
	    if (ent.Securing || ent.AlarmOwner != ply) then
		    return false
		end
	end
end)

hook.Add("PhysgunPickup", "DAlarmDisablePhysgunPickup", function(ply, ent)
    if (ent:GetClass() == "door_alarm") then
		if (ent.Securing || ent.AlarmOwner != ply) then
		    return false
		end
	end
end)

hook.Add("playerBoughtCustomEntity", "DAlarmOwner", function(ply, tab, ent)
	ent.AlarmOwner = ply //There's probably a DarkRP thing to set owners, but I couldn't find it and this does the job just perfect.
end)

hook.Add("onKeysLocked", "DAlarmActivate", function(ent)
	if (ent.Secured) then 
	    if (ent.Secured:GetNWString("DAlarm_Status") != "Active" && ent.Secured:GetNWString("DAlarm_Status") == "Inactive") then
            ent.Secured:SetNWString("DAlarm_Status", "Active")
		    ent.Secured:EmitSound("buttons/button17.wav")
		end
	end
end)

hook.Add("onKeysUnlocked", "DAlarmDeactivate", function(ent)
    if (ent.Secured) then 
	    if (ent.Secured:GetNWString("DAlarm_Status") == "Active" && ent.Secured:GetNWString("DAlarm_Status") != "Inactive") then
           ent.Secured:SetNWString("DAlarm_Status", "Inactive")
		end
	end
end)

hook.Add("playerSellDoor", "DAlarmRemoveFromUnownedDoor", function(ply, ent)
	if (ent.Secured) then
	    DarkRP.notify(ply, 1, 5, "Uncoupling alarm from sold door")
		ent.Secured:AlarmUncouple(ply)
	end
end)

hook.Add("PlayerDisconnected", "DAlarmRemoveUnownedAlarm", function(ply) //I know, I can use a DarkRP thing to set owners and it will automatically be removed when the owner leaves. I'll change all of this in the near future! Right now, I don't have time to do anything more than workarounds.
	for _, alarm in pairs(ents.FindByClass("door_alarm")) do
		if (alarm.AlarmOwner == ply) then
            alarm:AlarmExplosion()
		end
	end
end)

--[[ Dropbox is bugged, can't share files anymore! No update checking for now!
hook.Add("InitPostEntity", "DAlarmCheckUpdates", function()
	http.Fetch("", 
		function(version)   
	        if (version > "1.0.2") then 
			    MsgN(HUD_PRINTTALK, "[Door Alarm]: Outdated Version DETECTED!")
			end
		end,
		
	    function(error)
		    MsgN(HUD_PRINTTALK, "[Door Alarm]: Failed to check for UPDATES! ("..error..")")
	    end
	)
end)
]]

DarkRP.defineChatCommand("flipalarm", function(ply)
    local ent = ply:GetEyeTrace().Entity

	if (ent:GetClass() == "prop_door_rotating") then
		if (ent.Secured) then
			if (ent.Secured.AlarmOwner == ply) then
	            if (ent.Secured:GetNWString("DAlarm_Status") != "Active" && ent.Secured:GetNWString("DAlarm_Status") != "Inactive") then
 		            DarkRP.notify(ply, 1, 5, "You cannot flip this alarm now!")
				else
				    local ang = ent:GetAngles()
		            local pos = ent:GetPos()
	
	                local dAlarm = ents.Create("door_alarm")

			        if (ent.Secured.Flipped) then
			            dAlarm:SetAngles(ang)
			            dAlarm:SetPos(pos +ang:Right() *-40 +ang:Up() *1 +ang:Forward() *-1.5)
				        dAlarm.Flipped = false
			        else
		                ang:RotateAroundAxis(ang:Up(), 180)
			
			            dAlarm:SetAngles(ang)
			            dAlarm:SetPos(pos +ang:Right() *40 +ang:Up() *1 +ang:Forward() *-1.5)
				        dAlarm.Flipped = true
			        end
			        
					dAlarm:EmitSound("npc/dog/dog_servo12.wav")
			        dAlarm:SetHealth(ent.Secured:Health())
		 	        dAlarm:SetParent(ent)
			        dAlarm:Spawn()
			       
					dAlarm.AlarmOwner = ent.Secured.AlarmOwner
			        dAlarm.Securing = ent
			        dAlarm.DuringLockpick = false
			
			        ent.Secured:Remove()
			        ent.Secured = dAlarm
			
			        if (ent:isLocked()) then
				        dAlarm:SetNWString("DAlarm_Status", "Active")
			        else
			            dAlarm:SetNWString("DAlarm_Status", "Inactive")
			        end
		        end
	        else
			    DarkRP.notify(ply, 1, 5, "You do not own this alarm!")	
			end
		else
            DarkRP.notify(ply, 1, 5, "This door does not have an alarm!")		
        end
	else
	    DarkRP.notify(ply, 1, 5, "You need to be looking at a door!")
	end
	
	return ""
end)